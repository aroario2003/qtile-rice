;; ** variables and polls

(defpoll year
         :interval "1000s"
         "date +%Y")

(defpoll month
         :interval "1000s"
         "$(( $(date +%m) - 1 ))")

(defpoll day
         :interval "1000s"
         "date +%d")

(defpoll volume
         :interval "1s"
         "/home/legend/.local/bin/getVol")

(defpoll brightness
         :interval "1s"
         "/home/legend/.local/bin/getBright")

;; ** widgets

(defwidget controlcenter []
           (box :class "background"
                :hexpand "false"))

(defwidget brighticon []
           (box :orientation "h"
                :hexpand "false"
                (label :class "brightnessicon"
                       :text '')))

(defwidget brightscale []
           (box :class "brightnessbar"
                :space-evenly "true"
                :orientation "v"
                :spacing 0
                :hexpand "false"
                :vexpand "false"
                (scale :value brightness
                       :min 0
                       :max 101
                       :onchange "brillo -S {}")))

(defwidget volicon []
           (box :orientation "h"
                :hexpand "false"
                (label :class "volumeicon"
                       :text "")))

(defwidget volscale []
           (box :class "volumebar"
                :space-evenly "true"
                :orientation "v"
                :spacing 0
                :hexpand "false"
                :vexpand "false"
                (scale :value volume
                       :min 0
                       :max 101
                       :onchange "amixer -D pulse sset Master {}%")))

(defwidget cal []
           (box :orientation "h"
                :halign "center"
           (calendar :class "cal"
                     :month month
                     :day day
                     :year year)))

(defwidget closecal []
           (box :orientation "h"
                :space-evenly "true"
            (button :class "calbut" :onclick "eww close-all"
             (label :class "callabel"
                    :text 'close'))))

(defwidget byemsg []
           (box :orientation "h"
                (label :class "byebye"
                       :text 'Goodbye Legend')))

(defwidget powerbutton []
    (box :orientation "v"
         :space-evenly "true"
        (button :class "powerbut" :onclick "poweroff" "")
         (label :class "powerlabel"
                :text 'shutdown')))

(defwidget restartbutton []
          (box :orientation "v"
               :space-evenly "true"
           (button :class "restartbut" :onclick "reboot" "")
            (label :class "restartlabel"
                   :text 'restart')))

(defwidget logoutbutton []
           (box :orientation "v"
                :space-evenly "true"
            (button :class "logoutbut" :onclick "kill -9 -1" "")
             (label :class "logoutlabel"
                    :text 'logout')))

(defwidget closewindow []
           (box :orientation "v"
                :space-evenly "true"
            (button :class "closebut" :onclick "eww close-all" "")
             (label :class "closelabel"
                    :text 'close')))

(defwidget menu []
           (box :class "menu"
                :orientation "h"
                (powerbutton)
                (restartbutton)
                (logoutbutton)
                (closewindow)))

;; ** windows

(defwindow controlwin
           :geometry (geometry :x "1245px"
                               :y "35px"
                               :anchor "top left"
                               :width "210px"
                               :height "125px")
                               (controlcenter))

(defwindow brighticonwin
           :geometry (geometry :x "1245px"
                               :y "95px"
                               :anchor "top left"
                               :width "50px"
                               :height "60px")
                               (brighticon))

(defwindow brightwin
           :geometry (geometry :x "1300px"
                               :y "95px"
                               :anchor "top left"
                               :width "150px"
                               :height "60px")
                               (brightscale))

(defwindow voliconwin
           :geometry (geometry :x "1245px"
                               :y "35px"
                               :anchor "top left"
                               :width "50px"
                               :height "60px")
                               (volicon))

(defwindow volwin
           :geometry (geometry :x "1300px"
                               :y "35px"
                               :anchor "top left"
                               :width "150px"
                               :height "60px")
                               (volscale))

(defwindow calendar
           :geometry (geometry :x "1650px"
                               :y "30px"
                               :anchor "top left"
                               :width "100px"
                               :height "100px")
                               (cal))

(defwindow calclosewin
           :geometry (geometry :x "1650px"
                               :y "212px"
                               :anchor "top left"
                               :width "100px"
                               :height "30px")
                               (closecal))

(defwindow powermenu
           :monitor 0
           :geometry (geometry :x "600px"
                               :y "500px"
                               :anchor "top left"
                               :width "37%"
                               :height "15%")
                               (menu))

(defwindow messagewin
           :monitor 0
           :geometry (geometry :x "600px"
                               :y "320px"
                               :anchor "top left"
                               :width "37%"
                               :height "14%")
                               (byemsg))
