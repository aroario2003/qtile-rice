use anyhow::Result;
use serde::Serialize;
use axum::{Router, routing::get};
use serde_json::json;
use std::path::Path;
use std::env;
use std::fs;

#[derive(Debug, Serialize)]
pub struct Class {
    classname: String,
    instance_variables: Vec<String>,
    functions: Vec<String>,
}

#[allow(unused_assignments)]
pub(crate) fn init_class(path: &str) -> Result<Vec<Class>> {
    let mut class_vec: Vec<Class> = Vec::new(); 
    let mut content = String::new();
    if let true = Path::exists(Path::new(&path)) {
        if Path::is_file(Path::new(&path)) {
           let mut classname = String::new();
           if let Ok(name) = crate::parse_class_name(&path) {
               classname = name; 
           }

           let mut instance_variables: Vec<String> = Vec::new();
           if let Ok(vars) = crate::parse_instance_variables(&path) {
               instance_variables = vars;
           }

           let mut functions: Vec<String> = Vec::new();
           if let Ok(funcs) = crate::parse_functions(&path) {
               if let Ok(uml_funcs) = crate::format_func_to_uml(&path, funcs) {
                   functions = uml_funcs;
               }
            }

           let class = Class {
               classname: classname,
               instance_variables: instance_variables,
               functions: functions, 
           };

           class_vec.push(class);

        } else if Path::is_dir(Path::new(&path)) {
            let files = fs::read_dir(&path)?;
            for file in files {
                match file {
                    Ok(path) => {
                        if path.path().is_dir() {
                            init_class(&path.path().display().to_string())?;
                        } else if path.path().is_file() {
                            content = fs::read_to_string(path.path().display().to_string())?;
                            if content.is_empty() {
                                continue;
                            } else {
                                let mut classname = String::new();
                                if let Ok(name) = crate::parse_class_name(&path.path().display().to_string()) {
                                    classname = name; 
                                }

                                let mut instance_variables: Vec<String> = Vec::new();
                                if let Ok(vars) = crate::parse_instance_variables(&path.path().display().to_string()) {
                                    if let Ok(format_vars) = crate::format_instance_variables_to_uml(vars) {
                                        instance_variables = format_vars;
                                    }
                                }

                                let mut functions: Vec<String> = Vec::new();
                                if let Ok(funcs) = crate::parse_functions(&path.path().display().to_string()) {
                                    if let Ok(uml_funcs) = crate::format_func_to_uml(&path.path().display().to_string(), funcs) {
                                        functions = uml_funcs;
                                    }
                                }
                                let class = Class {
                                    classname: classname,
                                    instance_variables: instance_variables,
                                    functions: functions,
                                };

                                class_vec.push(class);
                            }
                        } 
                    },
                    Err(e) => {
                        println!("{}", e);
                    }
                }
           }
        } else {
            panic!("Invalid type of file");
        }
    } else {
        panic!("The path: {} does not exist", path);
    } 
    return Ok(class_vec);
}

pub async fn start_server(path: &str) {
   get_data(path).expect("could not get class information");
   let app = Router::new().route("/", get(root)); 

   let server = axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service());
   let addr = server.local_addr();
   println!("Listening on {addr}");
   server.await.unwrap();
}

fn get_data(path: &str) -> Result<()> {
    let mut class_vec = Vec::new();
    if let Ok(res) = init_class(path) {
        class_vec = res;
    }
    let home_dir = env::var("HOME")?;
    let json = json!(class_vec);
    let json_str = json.to_string();
    fs::write(home_dir + "/.local/share/umlrs/tmp.json", json_str)?; 
    Ok(())
}

async fn root() -> String {
    let home_dir = env::var("HOME");
    let content = fs::read_to_string(home_dir.unwrap() + "/.local/share/umlrs/tmp.json");
    if let Ok(json_str) = content {
        return json_str;
    } else {
        return String::from("");
    }
}
