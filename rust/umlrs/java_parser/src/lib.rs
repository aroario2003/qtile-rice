use anyhow::Result;
use cli_table::{format::Justify, Cell, Style, Table};
use git2::Repository;
use regex::Regex;
use std::env;
use std::fmt;
use std::fs;
use std::path::Path;
use std::path::PathBuf;
use std::process;

mod api;

enum Uml {
    Public,
    Private,
    Protected,
}

struct UmlType {
    public: Uml,
    private: Uml,
    protected: Uml,
}

impl Default for UmlType {
    fn default() -> Self {
        UmlType {
            public: Uml::Public,
            private: Uml::Private,
            protected: Uml::Protected,
        }
    }
}

impl fmt::Display for Uml {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Uml::Public => write!(f, "+"),
            Uml::Private => write!(f, "-"),
            Uml::Protected => write!(f, "#"),
        }
    }
}

//This function checks if a given class is an enum.
pub(crate) fn check_if_class_is_enum(path: &str) -> Result<bool> {
    let content = fs::read_to_string(&path)?;
    let lines = content.lines().into_iter().collect::<Vec<&str>>();
    if lines[0].contains("enum") {
        return Ok(true);
    }
    Ok(false)
}

//This function gets the classname or enum name from any java file. In order for this to work
//convention of the filename and classname being the same must be followed.
pub(crate) fn parse_class_name(path: &str) -> Result<String> {
    //get the classname of the given file in the path
    let dirs = path.split("/");
    let dirs_vec = dirs.into_iter().collect::<Vec<&str>>();
    let filename = dirs_vec[dirs_vec.len() - 1];
    let parts = filename.split_once(".");
    match parts {
        Some((classname, _)) => {
            //check if the class is a enum and add corresponding stereotype if so
            if let Ok(true) = check_if_class_is_enum(path) {
                let new_enum_name = format!("<<enumeration>>\n{}", classname);
                return Ok(new_enum_name);
            } else {
                Ok(classname.to_string())
            }
        }
        None => {
            println!("Could not parse out classname");
            process::exit(1);
        }
    }
}

//This funciton matches all method declarations including constructors and
//return them in a vector so that format_func_to_uml() can format them
#[allow(unused_assignments)]
pub(crate) fn parse_functions(path: &str) -> Result<Vec<String>> {
    let mut content = String::new();
    //make sure that the given path exists
    let exists = Path::exists(&PathBuf::from(&path));
    match exists {
        true => {
            content = fs::read_to_string(&path)?;
        }
        false => {
            println!("The path: {} does not exist", &path);
            process::exit(1);
        }
    }

    //capture all method declarations in the file
    let method = Regex::new(
        r"(public|protected|private|static|synchronized|\s) +[\w<.*>\[\]]+\s+(\w+) *\([^\)]*\) *(\{?|[^;])",
    );
    let mut functions: Vec<String> = Vec::new();
    match method {
        Ok(regex) => {
            //check if the given file has an enum wihtin it instead of a class
            if let Ok(true) = check_if_class_is_enum(path) {
                let mut enum_variants = content
                    .lines()
                    .into_iter()
                    .filter(|line| !line.contains("{") && !line.contains("}"))
                    .map(|line| line.to_string())
                    .collect::<Vec<String>>();
                enum_variants = enum_variants
                    .iter()
                    .map(|var| var.trim().replace(",", ""))
                    .collect();
                return Ok(enum_variants);
            } else {
                //collect the funcitons into a vector if they are not an enum
                functions = content
                    .lines()
                    .into_iter()
                    .filter(|line| regex.is_match(line))
                    .filter(|line| !line.contains("if") && !line.contains("else"))
                    .map(|line| line.trim())
                    .map(|line| line.to_string())
                    .collect::<Vec<_>>();
            }
        }
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        }
    }
    Ok(functions)
}

//This function takes all the fucntions which were found with parse_functions() and formats them
//into a uml friendly format so that the functions can than be displayed in a uml table.
pub(crate) fn format_func_to_uml(path: &str, mut functions: Vec<String>) -> Result<Vec<String>> {
    if let Ok(true) = check_if_class_is_enum(path) {
        for (idx, mut func) in functions.clone().into_iter().enumerate() {
            func.push_str("\n");
            func.to_string();
            functions[idx] = func;
        }
        Ok(functions)
    } else {
        let uml_types = UmlType::default();
        let mut formatted_functions = functions
            .iter()
            .map(|func| func.replace("public", &uml_types.public.to_string()))
            .map(|func| func.replace("private", &uml_types.private.to_string()))
            .map(|func| func.replace("protected", &uml_types.protected.to_string()))
            .map(|func| func.replace(" {", ""))
            .map(|func| func.replace("}", ""))
            .collect::<Vec<_>>();

        //capture all instances of parameters to functions
        let contains_in_parens =
            Regex::new(r"\(([a-zA-Z]+\[?.*?\]?+<?[a-zA-Z]+?>?,?\s?[a-zA-Z]+?\[?.*?\]?)+\)");
        match contains_in_parens {
            Ok(regex) => {
                for function in formatted_functions.clone().into_iter() {
                    let matches = regex.captures(function.as_str());
                    let orig_function = function.clone();
                    match matches {
                        Some(matchr) => {
                            let capture = &matchr[0];
                            let mut params = capture.split(",").collect::<Vec<_>>();
                            let mut new_params: Vec<String> = Vec::new();
                            for param in params.iter() {
                                //reversing parameter order so String something is something:String
                                let mut items = param.split_whitespace().collect::<Vec<_>>();
                                if items.len() == 2 {
                                    items.insert(items.len() / 2, ":");
                                    items.reverse();
                                    let new_items = items
                                        .iter()
                                        .map(|x| x.replace("(", "").replace(")", ""))
                                        .collect::<Vec<_>>();
                                    new_params.push(new_items.concat());
                                }
                            }
                            //insert commas in the correct places as they were removed when
                            //formatting parameters
                            params = new_params.iter().map(|it| it.as_str()).collect::<Vec<_>>();
                            let str_params = params
                                .into_iter()
                                .map(|item| item.to_string())
                                .collect::<Vec<String>>();
                            let mut new_params: Vec<String> = Vec::new();
                            for (idx, mut param) in str_params.clone().into_iter().enumerate() {
                                if idx != str_params.len() - 1 {
                                    param.push_str(", ");
                                }
                                new_params.push(param);
                            }
                            //concatenate the vector of formatted parameters together and create
                            //the new funciton with correct parameter formatting
                            let mut func_params = new_params.concat();
                            func_params.push_str(")");
                            func_params.insert(0, '(');
                            let new_function = function.replace(capture, &func_params);
                            let func_idx = formatted_functions
                                .iter()
                                .position(|x| x.eq(&orig_function));
                            match func_idx {
                                Some(idx) => {
                                    formatted_functions[idx] = new_function;
                                }
                                None => {
                                    println!("Index does not exist in the functions captured");
                                    process::exit(1);
                                }
                            }
                        }
                        None => {
                            continue;
                        }
                    }
                }
            }
            Err(e) => {
                println!("{}", e);
                process::exit(1);
            }
        }

        //make sure that the return type of the function is in the correct place at the end after a
        //colon and add a new line after every function
        for (idx, mut function) in formatted_functions.clone().into_iter().enumerate() {
            if function.contains("static") {
                let mut func_parts = function.split(" ").collect::<Vec<_>>();
                for (idx, part) in func_parts.clone().into_iter().enumerate() {
                    if part.eq("throws") {
                        func_parts.remove(idx);
                        func_parts.remove(idx);
                    }
                }
                if !func_parts[1].contains("(") && !func_parts[1].contains(")") {
                    func_parts.push(": ");
                    func_parts.push(func_parts[2]);
                    func_parts.remove(2);
                    func_parts.remove(1);
                    func_parts.insert(1, " ");
                    func_parts.insert(0, "\x1b[4m");
                    func_parts.insert(func_parts.len(), "\x1b[0m");
                    function = func_parts.concat();
                }
                function.push_str("\n");
                formatted_functions[idx] = function;
            } else {
                let mut func_parts = function.split(" ").collect::<Vec<_>>();
                for (idx, part) in func_parts.clone().into_iter().enumerate() {
                    if part.eq("throws") {
                        func_parts.remove(idx);
                        func_parts.remove(idx);
                    }
                }
                if !func_parts[1].contains("(") && !func_parts[1].contains(")") {
                    func_parts.push(": ");
                    func_parts.push(func_parts[1]);
                    func_parts.remove(1);
                    func_parts.insert(1, " ");
                    function = func_parts.concat();
                }
                function.push_str("\n");
                formatted_functions[idx] = function;
            }
        }

        Ok(formatted_functions)
    }
}

#[allow(unused_assignments)]
pub(crate) fn parse_instance_variables(path: &str) -> Result<Vec<String>> {
    let mut content = String::new();
    if let true = Path::exists(Path::new(&path)) {
        content = fs::read_to_string(&path)?;
    } else {
        println!("The path: {} does not exist", path);
        process::exit(1);
    }

    let re = Regex::new(
        r"(public|protected|private)\s(static)?\s?[a-zA-Z]+<?[a-zA-Z]+?>?\s[a-zA-Z]+\s?=?\s?[a-zA-Z0-9]+?\s?[a-zA-Z0-9]+?\(?.*?\)?;",
    );
    let mut instance_variables = Vec::new();

    if let Ok(true) = check_if_class_is_enum(&path) {
        instance_variables = vec![];
        return Ok(instance_variables);
    } else {
        match re {
            Ok(regex) => {
                instance_variables = content
                    .lines()
                    .filter(|line| regex.is_match(line))
                    .map(|line| line.trim())
                    .map(|line| line.to_string())
                    .collect();
            }
            Err(e) => {
                println!("{}", e);
                process::exit(1);
            }
        }
    }
    Ok(instance_variables)
}

pub(crate) fn format_instance_variables_to_uml(
    instance_variables: Vec<String>,
) -> Result<Vec<String>> {
    if instance_variables.is_empty() {
        return Ok(instance_variables);
    }
    let uml_types = UmlType::default();
    //replace all instances of visibility modifier keywords witht their corresponding symbols
    let mut formatted_vars = instance_variables
        .iter()
        .map(|var| var.replace("public", &uml_types.public.to_string()))
        .map(|var| var.replace("private", &uml_types.private.to_string()))
        .map(|var| var.replace("protected", &uml_types.protected.to_string()))
        .collect::<Vec<String>>();
    for (idx, mut var) in formatted_vars.clone().into_iter().enumerate() {
        if !var.contains("static") {
            //format instance variables if the instance varible is not static
            let mut original_var = var.clone();
            let mut var_parts = var.split(" ").collect::<Vec<_>>();

            let mut var_type: &str = "";
            if !var.contains("final") {
                var_type = var_parts[1];
            } else {
                var_type = var_parts[2];
                original_var = original_var.replace("final", "").replace("  ", " ");
            }

            original_var = original_var.replace(";", "");
            original_var.push_str(": ");
            original_var = original_var.replace(var_type, "");
            original_var.push_str(var_type);
            original_var.push_str("\n");
            original_var = original_var.replace("  ", " ");

            var = original_var;
            formatted_vars[idx] = var;
        } else if var.contains("static") {
            //format instance variables if the instance varible is static
            let mut original_var = var.clone();
            let mut var_parts = var.split(" ").collect::<Vec<_>>();
            
            let mut var_type: &str = "";
            if !var.contains("final") {
                var_type = var_parts[2];
            } else {
                var_type = var_parts[3];
                original_var = original_var.replace("final", "").replace("  ", " ");
            }

            if original_var.contains("=") {
                let mut parts = original_var.split(" =").collect::<Vec<&str>>();
                parts.remove(parts.len() - 1);
                original_var = parts.concat();
            }
            original_var = original_var.replace("static", "").replace("  ", " ");
            original_var = original_var.replace(";", "");
            original_var.push_str(": ");
            original_var = original_var.replace(var_type, "");
            original_var.push_str(var_type);
            original_var.push_str("\n");
            original_var = original_var.replace("  ", " ");
            original_var = String::from("\x1b[4m") + &original_var + &String::from("\x1b[0m");

            var = original_var;
            formatted_vars[idx] = var;
        }
    }
    Ok(formatted_vars)
}

pub fn get_classname(path: &str) -> String {
    if let Ok(classname) = parse_class_name(&path) {
        return classname;
    }
    String::from("")
}

pub fn get_functions(path: &str) -> String {
    let functions = parse_functions(path);
    let mut functions_str = String::new();
    if let Ok(functions) = functions {
        let formatted_functions = format_func_to_uml(path, functions);
        if let Ok(formatted_functions) = formatted_functions {
            functions_str = formatted_functions.join("\n");
        }
    }
    functions_str 
}

pub fn get_instance_variables(path: &str) -> String {
    let instance_vars = parse_instance_variables(path);
    let mut vars_str = String::new();
    if let Ok(instance_vars) = instance_vars {
        let formatted_vars = format_instance_variables_to_uml(instance_vars);
        if let Ok(formatted_vars) = formatted_vars {
            vars_str = formatted_vars.join("\n");
        }
    }
    vars_str
}

#[allow(unused_assignments)]
pub fn print_uml(path: &str) {
    //make a string which holds the classname and make sure that the file has a classname
    let mut class_name = String::new();
    if let Ok(classname) = parse_class_name(&path) {
        class_name = classname;
    }
    //parse the functions out of the file and make sure that they exist and format them into uml
    //format and print all necessary information in a neat table
    let functions = parse_functions(&path);
    let mut formatted_functions = String::new();
    let mut instance_vars = String::new();
    match functions {
        Ok(func_vec) => {
            if let Ok(funcs) = format_func_to_uml(&path, func_vec) {
                formatted_functions = funcs.concat();
            } else {
                println!("No functions could be formatted to uml in file");
                process::exit(1);
            }

            if let Ok(vars) = parse_instance_variables(&path) {
                if let Ok(formatted_vars) = format_instance_variables_to_uml(vars) {
                    instance_vars = formatted_vars.concat();
                }
            }

            if !instance_vars.is_empty() {
                let table = vec![
                    vec![instance_vars.cell().justify(Justify::Left)],
                    vec![formatted_functions.cell().justify(Justify::Left)],
                ]
                .table()
                .title(vec![class_name.cell().bold(true).justify(Justify::Center)]);

                let print_table = table.display().unwrap();
                println!("{}", print_table);
            } else if instance_vars.is_empty() {
                let table = vec![vec![formatted_functions.cell().justify(Justify::Left)]]
                    .table()
                    .title(vec![class_name.cell().bold(true).justify(Justify::Center)]);

                let print_table = table.display().unwrap();
                println!("{}", print_table);
            }
        }
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        }
    }
}

pub fn print_project_uml(path: &str) -> Result<()> {
    let files = fs::read_dir(&path)?;
    let mut java_files: Vec<String> = Vec::new();
    for file in files {
        match file {
            Ok(path) => {
                if path.path().is_dir() {
                    print_project_uml(&path.path().display().to_string())?;
                } else {
                    let path_string = path.path().display().to_string();
                    let path_parts = path_string.split("/").collect::<Vec<&str>>();
                    let filename = path_parts[path_parts.len() - 1];
                    let filename_parts = filename.split(".").collect::<Vec<&str>>();

                    if filename_parts[filename_parts.len() - 1].eq("java") {
                        java_files.push(path.path().display().to_string());
                    }
                }
            }
            Err(e) => {
                println!("{}", e);
                process::exit(1);
            }
        }
    }
    for file in java_files.iter() {
        let content = fs::read_to_string(file)?;
        if content.is_empty() {
            continue;
        } else {
            print_uml(file);
        }
    }
    Ok(())
}

pub fn print_repository_uml(url: &str) -> Result<()> {
    let mut location = String::new();
    let home_dir = env::var("HOME");
    if let Ok(home) = home_dir {
        location = home + "/.local/share/umlrs/";
        if let false = Path::exists(Path::new(&location)) {
            fs::create_dir_all(&location)?;
        }
    }
    let _repo = match Repository::clone(url, &location) {
        Ok(repo) => {
            if let Some(repo_name) = repo.namespace() {
                location = location + repo_name;
            }
        }
        Err(e) => panic!("Could not clone repository: {}", e),
    };
    print_project_uml(&location)?;

    Ok(())
}

pub async fn init_server(path: &str) -> Result<()> {
    api::start_server(path).await;
    Ok(())
}
