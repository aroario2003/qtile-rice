import http from "http";
import { Command } from "commander";
import fs from "fs";
import satori from "satori";
import { html } from "satori-html";

const program = new Command();
program
    .usage('[OPTIONS]...')
    .option('-s, --svg', 'Creates an svg in the html of the diagrams')
    .parse(process.argv);

let options = program.opts();

fetch("http://0.0.0.0:3000").then(response => response.text()).then(async function(text) {
    const json = JSON.parse(text);
    let classes = [];
    for (let i = 0; i < json.length; i++) {
        let classparts = [];
        let classname = json[i]["classname"];
        classname = classname.replace("<<enumeration>>", "&lt;&lt;enumeration&gt;&gt;<br>"); 
        classparts.push(classname);
        let functions = json[i]["functions"];
        classparts.push(functions);
        let instance_variables = json[i]["instance_variables"];
        classparts.push(instance_variables);
        classes.push(classparts);
    }
    let tables = [];
    if (options.svg) {
        for (let i = 0; i < classes.length; i++) {
            if (i == 0) {
                let table = `  
                    <style>
                        body {
                            background: black;
                        }

                        table {
                            border: 1px solid white;
                            border-collapse: collapse;
                        }
                 
                        th {
                            border: 1px solid white;
                            color: white;
                        }
    
                        td {
                            border: 1px solid white;
                            color: white;
                        }
                    </style>
                    <table>
                        <tr>
                            <th>${classes[i][0]}</th>
                        </tr>
                        <tr>
                            <td>${classes[i][2].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                        <tr>
                            <td>${classes[i][1].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                    </table>
                    <br></br>`;
                tables.push(table);
            } else {
                let table = `
                    <table>
                        <tr>
                            <th>${classes[i][0]}</th>
                        </tr>
                        <tr>
                            <td>${classes[i][2].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                        <tr>
                            <td>${classes[i][1].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                    </table>
                    <br></br>`;
                tables.push(table);
            }
        }
        let full = tables.join();

        const svg = html`
        <html>
        <svg xmlns="http://www.w3.org/2000/svg" width=2000 height=2000>
            <foreignObject x="10" y="10" width=2000 height=2000>
                <body xmlns="http://www.w3.org/1999/xhtml">
                    ${full}
                </body>
            </foreignObject>
        </svg>
        </html>`;

        let data = new ArrayBuffer();
        fetch("https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Mononoki/Regular/complete/Mononoki%20Nerd%20Font%20Complete%20Regular.ttf?raw=true").then(response => data = response.arrayBuffer());

        console.log(data);
        const formatted = await satori(svg, {
            width: 2000,
            height: 2000,
            fonts: [
                {
                    name: 'Mononoki',
                    data: data,
                    weight: 400,
                    style: 'normal',
                }
            ],
        });

        console.log(formatted.toString());

        fs.writeFile("svg.html", svg, (err) => {
            if (err) throw err;
            else {
                console.log("done");
            }
        });

    } else { 
        for (let i = 0; i < classes.length; i++) {
            if (i == 0) {
                let table = `
                    <style>
                        body {
                            background: black;
                        }
    
                        table {
                            border: 1px solid white;
                            border-collapse: collapse;
                        }
                 
                        th {
                            border: 1px solid white;
                            color: white;
                        }
    
                        td {
                            border: 1px solid white;
                            color: white;
                        }
                    </style>
                    <table>
                        <tr>
                            <th>${classes[i][0]}</th>
                        </tr>
                        <tr>
                            <td>${classes[i][2].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                        <tr>
                            <td>${classes[i][1].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                    </table>
                    <br></br>`;
                    tables.push(table);
            } else {
                let table = `
                    <table>
                        <tr>
                            <th>${classes[i][0]}</th>
                        </tr>
                        <tr>
                            <td>${classes[i][2].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                        <tr>
                            <td>${classes[i][1].join("<br>").replace("\x1b[4m", "<u>").replace("\x1b[0m", "</u>")}</td>
                        </tr>
                    </table>
                    <br></br>`;
                tables.push(table);
            }
        }
    }
    if (options.svg) {
        http.createServer(function (req, res) {
            res.end(svg);
        }).listen(8000, '0.0.0.0'); 
    } else {
    http.createServer(function (req, res) {
        res.end(tables.join(""));
    }).listen(8000, '0.0.0.0'); 
        
    }
})


