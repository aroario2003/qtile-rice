use gtk::prelude::*;
use gtk::glib;
use std::fs;

pub fn init_gtk_window(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);

    window.set_title("umlrs GUI");
    window.set_border_width(5);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(400, 200);

    let vbox = gtk::Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .border_width(12)
        .hexpand_set(false)
        .vexpand_set(false)
        .build();

    let hbox = gtk::Box::builder()
        .orientation(gtk::Orientation::Horizontal)
        .build();

    let file_button = gtk::Button::builder()
        .name("choose file")
        .label("Choose file")
        .margin_bottom(5)
        .margin_start(5)
        .margin_end(5)
        .margin_top(10)
        .hexpand_set(false)
        .vexpand_set(false)
        .build();

    let create_button = gtk::Button::builder()
        .name("create uml")
        .label("Create")
        .margin_bottom(5)
        .margin_start(5)
        .margin_top(10)
        .margin_end(5)
        .hexpand_set(false)
        .vexpand_set(false)
        .build();

    hbox.pack_start(&create_button, false, false, 5);

    let filename_label = gtk::Label::new(None);
    hbox.pack_end(&filename_label, false, false, 5);

    file_button.connect_clicked(glib::clone!(@weak window, @weak filename_label => move |_| {
        let file_type_win = gtk::Window::new(gtk::WindowType::Popup);
        file_type_win.set_default_size(200, 200);
        file_type_win.set_position(gtk::WindowPosition::Center);
        file_type_win.set_title("Choose file type");
        file_type_win.set_border_width(5);

        let text_vbox = gtk::Box::builder()
            .orientation(gtk::Orientation::Vertical)
            .border_width(5)
            .hexpand_set(false)
            .vexpand_set(false)
            .build();
        
        let button_hbox = gtk::Box::builder()
            .orientation(gtk::Orientation::Horizontal)
            .border_width(5)
            .hexpand_set(false)
            .vexpand_set(false)
            .build();
        
        let file_type_button = gtk::Button::builder()
            .name("file")
            .label("File")
            .margin_bottom(5)
            .margin_end(5)
            .margin_start(5)
            .margin_top(5)
            .hexpand_set(false)
            .vexpand_set(false)
            .build();

        let dir_button = gtk::Button::builder()
            .name("directory")
            .label("Directory")
            .margin_bottom(5)
            .margin_end(5)
            .margin_start(5)
            .margin_top(5)
            .hexpand_set(false)
            .vexpand_set(false)
            .build();

        button_hbox.pack_start(&file_type_button, false, false, 5);
        button_hbox.pack_end(&dir_button, false, false, 5);

        let text_label = gtk::Label::new(None);
        text_label.set_text("Choose a file type:");

        text_vbox.pack_start(&text_label, false, false, 5);
        text_vbox.pack_end(&button_hbox, false, false, 5);

        file_type_win.add(&text_vbox);

        file_type_win.show_all();

        file_type_button.connect_clicked(glib::clone!(@weak filename_label, @weak window, @weak file_type_win => move |_| {
            let file_chooser = gtk::FileChooserDialog::new(
                Some("Choose file"),
                Some(&window),
                gtk::FileChooserAction::Open
            );

            file_chooser.add_buttons(&[("Open", gtk::ResponseType::Accept), ("Cancel", gtk::ResponseType::Cancel)]);
            
            file_chooser.show_all();

            file_chooser.connect_response(glib::clone!(@weak filename_label => move |file_chooser, response| {
                if response == gtk::ResponseType::Accept {
                    let filename = file_chooser.filename();
                    let filename_text = filename
                        .unwrap()
                        .into_os_string()
                        .into_string()
                        .unwrap();
                    filename_label.set_text(&filename_text);
                }
                file_chooser.close();
            }));
            file_type_win.close();
        }));

        dir_button.connect_clicked(glib::clone!(@weak filename_label, @weak window, @weak file_type_win => move |_| {
            let file_chooser = gtk::FileChooserDialog::new(
                Some("Choose file"),
                Some(&window),
                gtk::FileChooserAction::SelectFolder
            );

            file_chooser.add_buttons(&[("Open", gtk::ResponseType::Accept), ("Cancel", gtk::ResponseType::Cancel)]);

            file_chooser.show_all();

            file_chooser.connect_response(glib::clone!(@weak filename_label => move |file_chooser, response| {
                if response == gtk::ResponseType::Accept {
                    let filename = file_chooser.filename();
                    let filename_text = filename
                        .unwrap()
                        .into_os_string()
                        .into_string()
                        .unwrap();
                    filename_label.set_text(&filename_text);
                }
                file_chooser.close();
            }));
            file_type_win.close();
        })); 
    })); 

    create_button.connect_clicked(glib::clone!(@weak filename_label => move |_| {
        if !filename_label.text().is_empty() {
            let classname= java_parser::get_classname(filename_label.text().as_str());
            let instance_variables = java_parser::get_instance_variables(filename_label.text().as_str());
            let functions = java_parser::get_functions(filename_label.text().as_str());

            let classname_label = gtk::Label::new(Some(classname.as_str()));
            let instance_variables_label = gtk::Label::new(Some(instance_variables.as_str()));
            let functions_label = gtk::Label::new(Some(functions.as_str()));

            let uml_grid = gtk::Grid::new();
            uml_grid.set_column_spacing(5);
            uml_grid.set_row_spacing(5);

            uml_grid.set_halign(gtk::Align::Center);
            uml_grid.set_valign(gtk::Align::Center);

            uml_grid.attach(&classname_label, 0, 0, 1, 1);
            uml_grid.attach(&instance_variables_label, 0, 1, 1, 1);
            uml_grid.attach(&functions_label, 0, 2, 1, 1);

            let content_win = gtk::Window::new(gtk::WindowType::Popup);
            content_win.set_title("umlrs GUI");
            content_win.set_default_size(500, 500);
            content_win.set_position(gtk::WindowPosition::Center);

            let scrollable = gtk::ScrolledWindow::default();
            content_win.add(&scrollable);

            let content_container = gtk::Box::builder()
                .orientation(gtk::Orientation::Vertical)
                .hexpand_set(false)
                .vexpand_set(false)
                .build();

            scrollable.add(&uml_grid);

            content_win.add(&content_container);
            content_win.show_all();
       } else {
            let warn_win = gtk::Window::new(gtk::WindowType::Popup);
            warn_win.set_title("File error");
            warn_win.set_default_size(200, 200);
            warn_win.set_position(gtk::WindowPosition::Center);
            warn_win.set_border_width(5);

            let vbox_container = gtk::Box::builder()
                .orientation(gtk::Orientation::Vertical)
                .hexpand_set(false)
                .vexpand_set(false)
                .border_width(5)
                .build();

            let warn_label = gtk::Label::new(None);
            warn_label.set_text("No file or directory has been selected");

            let ok_button= gtk::Button::builder()
                .name("ok")
                .label("Ok")
                .margin_end(5)
                .margin_start(5)
                .margin_top(5)
                .margin_bottom(5)
                .vexpand_set(false)
                .hexpand_set(false)
                .build();

            ok_button.connect_clicked(glib::clone!(@weak warn_win => move |_| {
               warn_win.close();
            }));

            vbox_container.pack_start(&warn_label, false, false, 5);
            vbox_container.pack_end(&ok_button, false, false, 5);

            warn_win.add(&vbox_container);
            warn_win.show_all();
        }
    }));

    let instruction_text = gtk::Label::new(None);
    instruction_text.set_markup("<big><b>Choose a file or directory to generate a uml diagram for:</b></big>");
    
    hbox.pack_start(&file_button, false, false, 5);

    vbox.pack_end(&hbox, false, false, 5);
    vbox.pack_start(&instruction_text, false, false, 5);

    window.add(&vbox);

    window.show_all();
}
