use clap::{Arg, ArgAction, Command};
use java_parser;
use std::path::Path;
use anyhow::Result;
use gtk::prelude::*;
use umlrs;

#[tokio::main]
async fn main() -> Result<()>{
    let flags = Command::new("umlrs")
        .about("A program to make uml diagrams from any programming language")
        .version("0.0.1")
        .arg_required_else_help(true)
        .author("Alejandro Rosario")
        .arg(
            Arg::new("language")
                .short('l')
                .long("language")
                .action(ArgAction::Set)
                .num_args(1),
        )
        .arg(
            Arg::new("file")
                .short('f')
                .long("file")
                .help("Specify the file or directory to use (full path)")
                .action(ArgAction::Set)
                .num_args(1),
        )
        .arg(
            Arg::new("server")
                .short('s')
                .long("server")
                .help("start an http server to view the generated diagram in a web browser")
                .action(ArgAction::SetTrue)
                .num_args(0),
        )
        .arg(
            Arg::new("gui")
                .short('g')
                .long("gui")
                .help("Start the graphical interface for the program")
                .action(ArgAction::SetTrue)
                .num_args(0),
        )
        .get_matches();

    if flags.contains_id("language") && flags.contains_id("file") {
        let file = flags.get_one::<String>("file");
        let lang = flags.get_one::<String>("language");
        if let Some(lang) = lang {
            if let Some(file) = file {
                if lang.eq("java") {
                    if flags.get_flag("server") {
                        java_parser::init_server(file.as_str()).await?;
                   } 
                   if Path::is_file(Path::new(file.as_str())) {
                        java_parser::print_uml(file.as_str());
                    } else if Path::is_dir(Path::new(file.as_str())) {
                        java_parser::print_project_uml(file.as_str())?;
                    }
                } else {
                    println!("That language is not supported or does not exist");
                }
            }
        }
    } else if flags.get_flag("gui") {
        let app = gtk::Application::new(Some("com.test.umlrs"), Default::default());
        app.connect_activate(umlrs::init_gtk_window);
        let empty: Vec<String> = vec![];
        app.run_with_args(&empty);
    } 
    Ok(())
}
