use iced::application;
use iced::widget::{container, text, text_input, button, radio};
use crate::theme_changer::ThemeChanger;
use crate::theme_changer::CustomTheme;
use crate::gui::TypeTest;

impl application::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn appearance(&self, style: &Self::Style) -> application::Appearance {
        match style {
            CustomTheme::TokyoNight => application::Appearance {
                background_color: iced::Color::from_rgb8(26, 27, 38).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
            },
            _ => application::Appearance {
                background_color: iced::Color::from_rgb8(26, 27, 38).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
            }
        }
    }
}

impl container::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn appearance(&self, style: &Self::Style) -> container::Appearance {
       match style {
           CustomTheme::TokyoNight => container::Appearance {
                background: iced::Color::from_rgb8(26, 27, 38).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                ..Default::default()
           }, 
            CustomTheme::Everforest => container::Appearance {
                background: iced::Color::from_rgb8(30, 35, 38).into(),
                text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                ..Default::default()
           },
            CustomTheme::Catpuccin => container::Appearance {
                background: iced::Color::from_rgb8(30, 30, 46).into(),
                text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                ..Default::default()
            },
            CustomTheme::Nord => container::Appearance {
                background: iced::Color::from_rgb8(46, 52, 64).into(),
                text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                ..Default::default()
            }
        } 
    }
}

impl radio::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn active(&self, style: &Self::Style, is_selected: bool) -> radio::Appearance { 
        if is_selected {
            match style {
                CustomTheme::TokyoNight => radio::Appearance {
                    background: iced::Color::from_rgb8(26, 27, 38).into(),
                    dot_color: iced::Color::from_rgb8(122, 162, 247).into(),
                    text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(169, 177, 214).into()
                },
                CustomTheme::Everforest => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 35, 38).into(),
                    dot_color: iced::Color::from_rgb8(127, 187, 179).into(),
                    text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(211, 198, 170).into()
                },
                CustomTheme::Catpuccin => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 30, 46).into(),
                    dot_color: iced::Color::from_rgb8(137, 180, 250).into(),
                    text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(205, 214, 244).into()
                },
                CustomTheme::Nord => radio::Appearance {
                    background: iced::Color::from_rgb8(46, 52, 64).into(),
                    dot_color: iced::Color::from_rgb8(129, 161, 193).into(),
                    text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(216, 222, 233).into()
                }
            }
        } else {
            match style {
                CustomTheme::TokyoNight => radio::Appearance {
                    background: iced::Color::from_rgb8(26, 27, 38).into(),
                    dot_color: iced::Color::from_rgb8(192, 192, 192).into(),
                    text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(169, 177, 214).into()
                },
                CustomTheme::Everforest => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 35, 38).into(),
                    dot_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(211, 198, 170).into()
                },
                CustomTheme::Catpuccin => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 30, 46).into(),
                    dot_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(205, 214, 244).into()
                },
                CustomTheme::Nord => radio::Appearance {
                    background: iced::Color::from_rgb8(46, 52, 64).into(),
                    dot_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(216, 222, 233).into()
                }
            }
        }
    }

    fn hovered(&self, style: &Self::Style, is_selected: bool) -> radio::Appearance {
        if is_selected {
            match style {
                CustomTheme::TokyoNight => radio::Appearance {
                    background: iced::Color::from_rgb8(26, 27, 38).into(),
                    dot_color: iced::Color::from_rgb8(122, 162, 247).into(),
                    text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(169, 177, 214).into()
                },
                CustomTheme::Everforest => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 35, 38).into(),
                    dot_color: iced::Color::from_rgb8(127, 187, 179).into(),
                    text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(211, 198, 170).into()
                },
                CustomTheme::Catpuccin => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 30, 46).into(),
                    dot_color: iced::Color::from_rgb8(137, 180, 250).into(),
                    text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(205, 214, 244).into()
                },
                CustomTheme::Nord => radio::Appearance {
                    background: iced::Color::from_rgb8(46, 52, 64).into(),
                    dot_color: iced::Color::from_rgb8(129, 161, 193).into(),
                    text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(216, 222, 233).into()
                }
            }
        } else {
            match style {
                CustomTheme::TokyoNight => radio::Appearance {
                    background: iced::Color::from_rgb8(26, 27, 38).into(),
                    dot_color: iced::Color::from_rgb8(122, 162, 247).into(),
                    text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(169, 177, 214).into()
                },
                CustomTheme::Everforest => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 35, 38).into(),
                    dot_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(211, 198, 170).into()
                },
                CustomTheme::Catpuccin => radio::Appearance {
                    background: iced::Color::from_rgb8(30, 30, 46).into(),
                    dot_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(205, 214, 244).into()
                },
                CustomTheme::Nord => radio::Appearance {
                    background: iced::Color::from_rgb8(46, 52, 64).into(),
                    dot_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                    border_width: 1.0,
                    border_color: iced::Color::from_rgb8(216, 222, 233).into()
                }
            }
        }
    }
}

impl button::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn active(&self, style: &Self::Style) -> button::Appearance {
       match style {
           CustomTheme::TokyoNight => button::Appearance {
                background: iced::Color::from_rgb8(26, 27, 38).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                border_color: iced::Color::from_rgb8(192, 192, 192).into(),
                border_width: 1.0,
                ..Default::default()
           },
           CustomTheme::Everforest => button::Appearance {
                background: iced::Color::from_rgb8(30, 35, 38).into(),
                text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                border_color: iced::Color::from_rgb8(192, 192, 192).into(),
                border_width: 1.0,
                ..Default::default()
           },
           CustomTheme::Catpuccin => button::Appearance {
                background: iced::Color::from_rgb8(30, 30, 46).into(),
                text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                border_color: iced::Color::from_rgb8(192, 192, 192).into(),
                border_width: 1.0,
                ..Default::default()
           },
            CustomTheme::Nord => button::Appearance {
                background: iced::Color::from_rgb8(46, 52, 64).into(),
                text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                border_color: iced::Color::from_rgb8(192, 192, 192).into(),
                border_width: 1.0,
                ..Default::default()
            }
       }     
    }

    fn hovered(&self, style: &Self::Style) -> button::Appearance {
       match style {
           CustomTheme::TokyoNight => button::Appearance {
                background: iced::Color::from_rgb8(36, 38, 53).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                ..Default::default()
           },
           CustomTheme::Everforest => button::Appearance {
                background: iced::Color::from_rgb8(30, 35, 38).into(),
                text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                ..Default::default()
           },
           CustomTheme::Catpuccin => button::Appearance {
                background: iced::Color::from_rgb8(30, 30, 46).into(),
                text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                ..Default::default()
           },
           CustomTheme::Nord => button::Appearance {
                background: iced::Color::from_rgb8(46, 52, 64).into(),
                text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                ..Default::default()
          }
       }     
    }

    fn pressed(&self, style: &Self::Style) -> button::Appearance {
       match style {
           CustomTheme::TokyoNight => button::Appearance {
                background: iced::Color::from_rgb8(31, 26, 38).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                ..Default::default()
          },
           CustomTheme::Everforest => button::Appearance {
                background: iced::Color::from_rgb8(30, 35, 38).into(),
                text_color: iced::Color::from_rgb8(211, 198, 170).into(),
                ..Default::default()
           },
           CustomTheme::Catpuccin => button::Appearance {
                background: iced::Color::from_rgb8(30, 30, 46).into(),
                text_color: iced::Color::from_rgb8(205, 214, 244).into(),
                ..Default::default()
           },
           CustomTheme::Nord => button::Appearance {
                background: iced::Color::from_rgb8(46, 52, 64).into(),
                text_color: iced::Color::from_rgb8(216, 222, 233).into(),
                ..Default::default()
          }
       }     
    }

    fn disabled(&self, style: &Self::Style) -> button::Appearance {
       match style {
           CustomTheme::TokyoNight => button::Appearance {
                background: iced::Color::from_rgb8(19, 16, 23).into(),
                text_color: iced::Color::from_rgb8(169, 177, 214).into(),
                border_color: iced::Color::from_rgb8(192, 192, 192).into(),
                border_width: 1.0,
                ..Default::default()
          },
          CustomTheme::Everforest => button::Appearance {
               background: iced::Color::from_rgb8(30, 35, 38).into(),
               text_color: iced::Color::from_rgb8(211, 198, 170).into(),
               border_color: iced::Color::from_rgb8(192, 192, 192).into(),
               border_width: 1.0,
               ..Default::default()
          },
          CustomTheme::Catpuccin => button::Appearance {
               background: iced::Color::from_rgb8(30, 30, 46).into(),
               text_color: iced::Color::from_rgb8(205, 214, 244).into(),
               border_color: iced::Color::from_rgb8(192, 192, 192).into(),
               border_width: 1.0,
               ..Default::default()
          },
          CustomTheme::Nord => button::Appearance {
               background: iced::Color::from_rgb8(46, 52, 64).into(),
               text_color: iced::Color::from_rgb8(216, 222, 233).into(),
               border_color: iced::Color::from_rgb8(192, 192, 192).into(),
               border_width: 1.0,
               ..Default::default()
          }
       }     
    }
}

impl text::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn appearance(&self, style: Self::Style) -> text::Appearance{
        match style {
           CustomTheme::TokyoNight => text::Appearance {
                color: Some(iced::Color::from_rgb8(169, 177, 214))
            }, 
           CustomTheme::Everforest => text::Appearance {
                color: Some(iced::Color::from_rgb8(211, 198, 170))
            }, 
           CustomTheme::Catpuccin => text::Appearance {
                color: Some(iced::Color::from_rgb8(205, 214, 244))
            }, 
           CustomTheme::Nord => text::Appearance {
                color: Some(iced::Color::from_rgb8(216, 222, 233))
           } 
        } 
    }
}

impl text_input::StyleSheet for CustomTheme {
    type Style = CustomTheme;

    fn active(&self, style: &Self::Style) -> text_input::Appearance {
        match style {
            CustomTheme::TokyoNight => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            },
            _ => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            }
        }
    }

    fn focused(&self, style: &Self::Style) -> text_input::Appearance {
        match style {
            CustomTheme::TokyoNight => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            },
            _ => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            }
        }
    }

    fn disabled(&self, style: &Self::Style) -> text_input::Appearance {
        match style {
            CustomTheme::TokyoNight => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            },
            _ => text_input::Appearance {
               background: iced::Color::from_rgb8(26, 27, 38).into(),
               border_color: iced::Color::from_rgb8(122, 162, 247),
               border_width: 1.0,
               border_radius: 0.0, 
               icon_color: iced::Color::from_rgb8(192, 192, 192)
            }
        }
    }

    fn selection_color(&self, _style: &Self::Style) -> iced::Color { 
       iced::Color::from_rgb8(122, 162, 247) 
    }

    fn disabled_color(&self, _style: &Self::Style) -> iced::Color { 
        iced::Color::from_rgb8(26, 27, 38)
    }

    fn value_color(&self, _style: &Self::Style) -> iced::Color {
        iced::Color::from_rgb8(169, 177, 214)
    }

    fn placeholder_color(&self, _style: &Self::Style) -> iced::Color {
       iced::Color::from_rgb8(192, 192, 192)     
    }
}

