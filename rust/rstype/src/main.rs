use clap::{Arg, ArgAction, Command};

fn main() {
    let args = Command::new("rstype")
        .about("A commandline and gui typing test")
        .version("0.0.1")
        .arg_required_else_help(false)
        .author("Alejandro Rosario")
        .arg(
            Arg::new("wordlist")
                .short('W')
                .long("wordlist")
                .help("specify a wordlist to use")
                .action(ArgAction::Set)
                .num_args(1)
                .default_value("/usr/share/dict/words"),
        )
        .arg(
            Arg::new("num-words")
                .short('n')
                .long("num-words")
                .help("specify the number of words to give")
                .action(ArgAction::Set)
                .num_args(1)
                .default_value("50"),
        )
        .arg(
            Arg::new("word-length")
                .short('l')
                .long("word-length")
                .help("specify the word length to use")
                .action(ArgAction::Set)
                .num_args(1)
                .default_value("0"),
        )
        .arg(
            Arg::new("right-color")
                .short('r')
                .long("right-color")
                .help("specify the color to use when a letter is typed correctly")
                .action(ArgAction::Set)
                .num_args(1)
                .default_value("green"),
        )
        .arg(
            Arg::new("wrong-color")
                .short('w')
                .long("wrong-color")
                .help("specify the color to use when a letter is typed incorrectly")
                .action(ArgAction::Set)
                .num_args(1)
                .default_value("red"),
        )
        .get_matches();

    if args.args_present() {
        rstype::start_gui();
    } else {
        let generator = rstype::TextGenerator::new(
            args.get_one::<String>("wordlist").unwrap().as_str(),
            args.get_one::<String>("num-words")
                .unwrap()
                .parse::<i32>()
                .unwrap(),
            args.get_one::<String>("word-length")
                .unwrap()
                .parse::<i32>()
                .unwrap(),
        );

        let _ = rstype::typing(
            generator,
            generator.generate_text(),
            args.get_one::<String>("right-color").unwrap(),
            args.get_one::<String>("wrong-color").unwrap(),
        );
    }
}
