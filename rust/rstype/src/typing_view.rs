use iced::widget::{container, text, text_input, column};
use iced::Application;

use crate::gui::TypeTest;
use crate::TextGenerator;
use crate::gui::Message;
use crate::theme_changer::CustomTheme;

pub struct TypingView {
    pub text: String, 
    pub theme: CustomTheme,
    pub cur_pos: usize,
    pub cur_word: String
}

impl TypingView {
    pub fn new() -> TypingView {
        TypingView {
            text: TextGenerator::default().generate_text(),
            theme: CustomTheme::TokyoNight,
            cur_pos: 0,
            cur_word: String::from("")
        }
    }

    pub fn view(&self) -> iced::Element<Message, iced::Renderer<CustomTheme>> {
        let words = self.text
            .split(" ")
            .into_iter()
            .collect::<Vec<&str>>();
        self.cur_word = words[self.cur_pos].to_string();

        container(
            column(
                vec![
                    text(self.text.clone())
                        .style(self.theme)
                        .size(20)
                        .into(),
                        
                    text_input(self.cur_word.as_str(), "")
                        .on_input(Message::WordTyped)
                        .style(self.theme)
                        .into()
               ]
            )
        )
        .padding(10)
        .width(iced::Length::Fill)
        .height(iced::Length::Fill)
        .style(self.theme)
        .center_x()
        .center_y()
        .into()
    }
}
