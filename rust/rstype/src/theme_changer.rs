use iced::executor;
use iced::{Application, Command, Settings};
use iced::widget::{row, column, container, text, text_input, button, radio};

use crate::gui::Message;
use crate::gui::Views;
use crate::gui::TypeTest;

pub struct ThemeChanger {
    pub theme: CustomTheme
}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
pub enum CustomTheme {
    #[default]
    TokyoNight,
    Everforest,
    Catpuccin,
    Nord
}

impl ThemeChanger {
    pub fn new() -> Self {
        ThemeChanger {
            theme: CustomTheme::TokyoNight
        }
    }

    pub fn view(&self) -> iced::Element<Message, iced::Renderer<CustomTheme>> {
        container(
            column(
                vec![
                   text("Choose a theme:")
                        .style(self.theme)
                        .size(30)
                        .into(),
                   radio(
                       "Tokyo Night", 
                       CustomTheme::TokyoNight, 
                       Some(match self.theme {
                          CustomTheme::TokyoNight => CustomTheme::TokyoNight,
                          CustomTheme::Everforest => CustomTheme::Everforest,
                          CustomTheme::Catpuccin => CustomTheme::Catpuccin,
                          CustomTheme::Nord => CustomTheme::Nord,
                        }), 
                       Message::ThemeSelected
                    ).style(self.theme).into(),
                   radio(
                       "Everforest", 
                       CustomTheme::Everforest, 
                       Some(match self.theme {
                          CustomTheme::TokyoNight => CustomTheme::TokyoNight,
                          CustomTheme::Everforest => CustomTheme::Everforest,
                          CustomTheme::Catpuccin => CustomTheme::Catpuccin,
                          CustomTheme::Nord => CustomTheme::Nord,
                        }), 
                       Message::ThemeSelected
                    ).style(self.theme).into(),
                   radio(
                       "Catpuccin", 
                       CustomTheme::Catpuccin, 
                       Some(match self.theme {
                          CustomTheme::TokyoNight => CustomTheme::TokyoNight,
                          CustomTheme::Everforest => CustomTheme::Everforest,
                          CustomTheme::Catpuccin => CustomTheme::Catpuccin,
                          CustomTheme::Nord => CustomTheme::Nord,
                        }), 
                        Message::ThemeSelected
                    ).style(self.theme).into(),
                   radio(
                       "Nord", 
                       CustomTheme::Nord, 
                       Some(match self.theme {
                          CustomTheme::TokyoNight => CustomTheme::TokyoNight,
                          CustomTheme::Everforest => CustomTheme::Everforest,
                          CustomTheme::Catpuccin => CustomTheme::Catpuccin,
                          CustomTheme::Nord => CustomTheme::Nord,
                        }), 
                       Message::ThemeSelected
                    ).style(self.theme).into(),
                    container(
                        row(
                            vec![
                                button("Back")
                                    .on_press(Message::ChangeView(Views::Settings))
                                    .style(self.theme)
                                    .into()
                            ]
                        )
                    )
                    .width(iced::Length::Fill)
                    .padding(10)
                    .style(self.theme)
                    .align_x(iced::alignment::Horizontal::Right)
                    .into()
                ]
            )
        )
        .width(iced::Length::Fill)
        .height(iced::Length::Fill)
        .padding(10)
        .center_x()
        .center_y()
        .style(self.theme)
        .into()
    }
}


