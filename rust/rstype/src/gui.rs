use iced::executor;
use iced::{Application, Command, Settings};
use iced::widget::{row, column, container, text, text_input, button};
use std::process;

use crate::theme_changer::CustomTheme;
use crate::theme_changer;
use crate::theme_changer::ThemeChanger;
use crate::TextGenerator;
use crate::typing_view::TypingView;

#[derive(Clone, Debug, PartialEq)]
pub enum Message {
    SetWordlistValue(String),
    SetWordLengthValue(String),
    SetNumWordsValue(String),
    StartTypingTest,
    ChangeView(Views),
    ThemeSelected(CustomTheme),
    WordTyped(String)

}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Views {
    Settings,
    Theme, 
    TypingTest
}

pub struct TypeTest {
    wordlist: String,
    word_len: i32,
    num_words: i32,
    current_view: Views,
    theme_view: ThemeChanger,
    typing_view: TypingView,
    pub theme: CustomTheme
}

impl Application for TypeTest {
    type Message = Message;
    type Executor = executor::Default;
    type Flags = ();
    type Theme = CustomTheme; 

    fn new(_flags: ()) -> (Self, Command<Self::Message>) {
        (
        TypeTest { 
            wordlist: String::from("/usr/share/dict/words"), 
            word_len: 0, 
            num_words: 50, 
            current_view: Views::Settings, 
            theme_view: ThemeChanger::new(), 
            typing_view: TypingView::new(), 
            theme: CustomTheme::TokyoNight
        }, 
        Command::none()
        )
    }

    fn title(&self) -> String {
        String::from("rstype - GUI")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::SetWordlistValue(value) => {
                self.wordlist = String::from(value);
            },
            Message::SetWordLengthValue(length) => {
                if let Ok(length) = length.parse::<i32>() {
                    self.word_len = length;
                }
            },
            Message::SetNumWordsValue(number) => {
                if let Ok(number) = number.parse::<i32>() {
                    self.num_words = number;
                }
            },
            Message::StartTypingTest => {
                let generator = TextGenerator {
                    dict_file: self.wordlist.as_str(),
                    num_words: self.num_words,
                    word_len: self.word_len
                };
                self.typing_view.text = generator.generate_text();
                self.current_view = Views::TypingTest;
            },
            Message::ChangeView(page) => {
                self.current_view = page; 
            },
            Message::ThemeSelected(theme) => {
                self.theme = match theme {
                    CustomTheme::TokyoNight => CustomTheme::TokyoNight,
                    CustomTheme::Everforest => CustomTheme::Everforest,
                    CustomTheme::Catpuccin => CustomTheme::Catpuccin,
                    CustomTheme::Nord => CustomTheme::Nord
                };
                self.theme_view.theme = self.theme;
                self.typing_view.theme = self.theme;
            },
            Message::WordTyped(word) => {
                self.typing_view.cur_pos += 1;
            }
        };
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let settings_layout = container(
            column(
                vec![
                    container(
                        row(
                            vec![
                                text("Rstype Settings").size(50).into()
                            ]
                        )
                        .padding(10)
                        .spacing(10)
                    )
                    .center_x()
                    .width(iced::Length::Fill)
                    .style(self.theme)
                    .into(),

                    row(
                        vec![
                            text("path to wordlist").into(),
                            text_input("path to wordlist", self.wordlist.as_str()) 
                                     .on_input(Message::SetWordlistValue)
                                     .style(self.theme)
                                     .into()
                        ]   
                    )
                    .spacing(10)
                    .padding(10)
                    .into(),

                    row(
                        vec![
                            text("word length (0 means anything)").into(),
                            text_input(&self.word_len.to_string(), "")
                                    .on_input(Message::SetWordLengthValue)
                                    .style(self.theme)
                                    .into()
                        ] 
                    )
                    .padding(10)
                    .spacing(10)
                    .into(),

                    row(
                        vec![
                            text("number of words").into(),
                            text_input(&self.num_words.to_string(), "")
                                    .on_input(Message::SetNumWordsValue)
                                    .style(self.theme)
                                    .into()
                        ]
                    )
                    .padding(10)
                    .spacing(10)
                    .into(),

                    container(
                        row(
                            vec![
                                button("Change Theme")
                                    .on_press(Message::ChangeView(Views::Theme))
                                    .style(self.theme)
                                    .into(),

                                button("Ok")
                                    .on_press(Message::StartTypingTest)
                                    .style(self.theme)
                                    .into()
                            ]
                        )
                        .padding(5)
                        .spacing(5)
                    )
                    .width(iced::Length::Fill)
                    .padding(10)
                    .style(self.theme)
                    .align_x(iced::alignment::Horizontal::Right)
                    .into()
                ]
            )
        )
        .padding(10)
        .width(iced::Length::Fill)
        .height(iced::Length::Fill)
        .style(self.theme)
        .center_x()
        .center_y();

        match self.current_view {
            Views::Settings => settings_layout.into(),
            Views::Theme => self.theme_view.view(),
            Views::TypingTest => self.typing_view.view(),
        }
    }

   fn theme(&self) -> Self::Theme {
        self.theme.clone()     
   }
}


pub fn run() {
    let settings = Settings {
        window: iced::window::Settings {
            size: (505, 304),
            ..Default::default()
        },
        
        ..Default::default()
    };
   let _ = TypeTest::run(settings);
}



