mod gui;
mod theme;
mod theme_changer;
mod typing_view;

use rand::Rng;
use std::fs;
use std::io::{stdin, stdout, Write};
use std::thread;
use std::time;
use termion::cursor;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

pub struct Color<'a> {
    pub red: &'a str,
    pub green: &'a str,
    pub yellow: &'a str,
    pub blue: &'a str,
    pub orange: &'a str,
    pub purple: &'a str,
    pub white: &'a str,
}

impl Default for Color<'_> {
    fn default() -> Self {
        return Color {
            red: "\x1b[31m",
            green: "\x1b[32m",
            yellow: "\x1b[33m",
            blue: "\x1b[34m",
            orange: "\x1b[33m",
            purple: "\x1b[35m",
            white: "\x1b[0m",
        };
    }
}

impl Color<'_> {
    pub fn get(col: &str) -> &str {
        let colors = Color::default();
        if col.eq("red") {
            return colors.red;
        } else if col.eq("green") {
            return colors.green;
        } else if col.eq("yellow") {
            return colors.yellow;
        } else if col.eq("blue") {
            return colors.blue;
        } else if col.eq("orange") {
            return colors.orange;
        } else if col.eq("purple") {
            return colors.purple;
        } else {
            return colors.white;
        }
    }
}

#[derive(Copy, Clone)]
pub struct TextGenerator<'a> {
    pub dict_file: &'a str,
    pub num_words: i32,
    pub word_len: i32,
}

impl Default for TextGenerator<'_> {
    fn default() -> Self {
        return TextGenerator {
            dict_file: "./top250",
            num_words: 100,
            word_len: 0,
        };
    }
}

impl TextGenerator<'_> {
    pub fn new<'a>(dict_file: &'a str, num_words: i32, word_len: i32) -> TextGenerator<'a> {
        return TextGenerator {
            dict_file,
            num_words,
            word_len,
        };
    }

    pub fn generate_text(&self) -> String {
        if let Ok(contents) = fs::read_to_string(self.dict_file) {
            let words_vec = contents.split("\n").into_iter().collect::<Vec<&str>>();
            let mut num_words = 0;
            let mut words_to_ret: Vec<String> = vec![];
            let mut rng = rand::thread_rng();
            if self.word_len == 0 {
                for _ in 1..=self.num_words {
                    num_words += 1;
                    let rand_num = rng.gen_range(1..words_vec.len());
                    let word = words_vec[rand_num];
                    words_to_ret.push(word.to_string());
                    if num_words == 15 {
                        words_to_ret.push("\n".to_string());
                        num_words = 0;
                    }
                }
            } else if self.word_len > 0 {
                for _ in 1..=self.num_words {
                    num_words += 1;
                    let rand_num = rng.gen_range(1..words_vec.len());
                    let mut word = words_vec[rand_num];
                    while word.len() as i32 != self.word_len {
                        let rand_num = rng.gen_range(1..words_vec.len());
                        word = words_vec[rand_num];
                    }
                    words_to_ret.push(word.to_string());
                    if num_words == 15 {
                        words_to_ret.push("\n".to_string());
                        num_words = 0;
                    }
                }
            }
            let words_ret = words_to_ret.join(" ");
            return words_ret;
        } else {
            return "".to_string();
        }
    }
}

fn live_wpm(wpm: i32) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    let _ = writeln!(stdout, "{}", cursor::Goto(1, 1));
    let _ = writeln!(stdout, "wpm: {}", wpm);
}

fn live_time(current_time: time::Instant) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    thread::spawn(move || loop {
        let _ = writeln!(stdout, "{}", cursor::Goto(15, 1));
        let _ = writeln!(stdout, "time: {:?}", current_time.elapsed());
        stdout.flush().unwrap();
        thread::sleep(time::Duration::from_secs(1));
    });
}

pub fn calculate_wpm(typed_chars: f64, errors: f64, time: time::Duration) -> i32 {
    let time_minutes = time.as_secs_f64() / 60.0;
    let gross_wpm = (typed_chars / 5.0) / time_minutes;
    let net_wpm = gross_wpm - (errors / time_minutes);
    return net_wpm.abs() as i32;
}

#[allow(unused_assignments)]
pub fn typing(
    text_generator: TextGenerator,
    cur_str: String,
    right_color: &str,
    wrong_color: &str,
) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    let mut x = 0;
    let mut y = 0;
    let help_string = format!(
        "{}ctrl-c{} to quit and {}ctrl-r{} to restart{}",
        Color::get(right_color),
        Color::get(wrong_color),
        Color::get(right_color),
        Color::get(wrong_color),
        Color::get("white")
    );

    if let Ok((_, rows)) = termion::terminal_size() {
        y = rows / 2;
    }

    let _ = writeln!(stdout, "{}{}", termion::clear::All, cursor::Hide);

    let cur_str_parts = cur_str
        .split("\n")
        .into_iter()
        .map(|x| x.trim().to_string())
        .collect::<Vec<String>>();

    for mut part in cur_str_parts.clone() {
        part.push_str(" ");
        if let Ok((columns, _)) = termion::terminal_size() {
            x = ((columns as i32 - part.len() as i32) / 2) as u16;
        }
        let _ = writeln!(stdout, "{}", cursor::Goto(x, y));
        let _ = writeln!(stdout, "{}", part);
        stdout.flush().unwrap();
        y += 1;
    }

    if let Ok((columns, rows)) = termion::terminal_size() {
        x = ((columns as i32
            - help_string
                .replace(Color::get(right_color), "")
                .replace(Color::get(wrong_color), "")
                .len() as i32)
            / 2) as u16;
        y = rows - 1;
    }

    let _ = writeln!(stdout, "{}{}", cursor::Goto(x, y), help_string);

    if let Ok((_, rows)) = termion::terminal_size() {
        y = rows / 2;
    }

    let now = time::Instant::now();
    let mut typed_chars = 0.0;
    let mut errors = 0.0;
    let mut wpm = 0;
    let mut correct_chars = 0.0;

    live_time(now);

    for mut part in cur_str_parts {
        part.push_str(" ");
        if let Ok((columns, _)) = termion::terminal_size() {
            x = ((columns as i32 - part.len() as i32) / 2) as u16;
        }

        let part_str_arr = part.chars().collect::<Vec<char>>();
        let mut str_arr = part_str_arr
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        for (idx, ch) in part_str_arr.iter().enumerate() {
            str_arr.remove(idx);
            for c in stdin().keys() {
                if *c.as_ref().unwrap() == Key::Ctrl('c') {
                    let _ = writeln!(stdout, "{}", termion::clear::All);
                    let _ = writeln!(stdout, "{}", cursor::Restore);
                    let _ = writeln!(stdout, "{}", cursor::Goto(1, 1));
                    stdout.flush().unwrap();
                    std::process::exit(0);
                } else if *c.as_ref().unwrap() == Key::Ctrl('r') {
                    let _ = writeln!(stdout, "{}{}", termion::clear::All, cursor::Restore);
                    let cur_str = text_generator.generate_text();
                    typing(text_generator, cur_str, right_color, wrong_color);
                } else if *c.as_ref().unwrap() == Key::Backspace {
                    let removed = str_arr.remove(idx - 1);
                    str_arr.insert(
                        idx - 1,
                        removed
                            .replace(Color::get(right_color), "")
                            .replace(Color::get(wrong_color), ""),
                    );
                    part = str_arr.join("");
                    let _ = writeln!(stdout, "{}", cursor::Goto(x, y));
                    let _ = writeln!(stdout, "{}", part);
                    stdout.flush().unwrap();
                }
                match Key::Char(*ch) == c.unwrap() {
                    true => {
                        wpm = calculate_wpm(typed_chars, errors, now.elapsed());
                        live_wpm(wpm);

                        let highlighted_ch = Color::get(right_color).to_owned()
                            + &ch.to_string()
                            + Color::get("white");
                        str_arr.insert(idx, highlighted_ch);
                        part = str_arr.join("");
                        let _ = writeln!(stdout, "{}", cursor::Goto(x, y));
                        let _ = writeln!(stdout, "{}", part);
                        stdout.flush().unwrap();
                        typed_chars += 1.0;
                        correct_chars += 1.0;
                        break;
                    }

                    false => {
                        wpm = calculate_wpm(typed_chars, errors, now.elapsed());
                        live_wpm(wpm);

                        let highlighted_ch = Color::get(wrong_color).to_owned()
                            + &ch.to_string()
                            + Color::get("white");
                        str_arr.insert(idx, highlighted_ch);
                        part = str_arr.join("");
                        let _ = writeln!(stdout, "{}", cursor::Goto(x, y));
                        let _ = writeln!(stdout, "{}", part);
                        stdout.flush().unwrap();
                        typed_chars += 1.0;
                        errors += 1.0;
                        break;
                    }
                }
            }
        }
        y += 1;
    }
    let elapsed = now.elapsed();
    wpm = calculate_wpm(typed_chars, errors, time::Duration::from(elapsed));
    let acc_num = correct_chars / typed_chars;
    let accuracy = acc_num * 100.0;
    let _ = writeln!(stdout, "{}", termion::clear::All);
    let _ = writeln!(stdout, "{}", termion::cursor::Restore);
    let _ = writeln!(stdout, "{}", cursor::Goto(1, 1));
    stdout.flush().unwrap();
    println!("your wpm was: {wpm}");
    println!("your accuracy was: {}%", accuracy.round());
}

pub fn start_gui() {
    crate::gui::run();
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_calculate_wpm() {
        let typed_chars = 200.0;
        let errors = 10.0;
        let time = time::Duration::from_secs(60);
        let wpm = calculate_wpm(typed_chars, errors, time);
        assert_eq!(wpm, 30);

        let typed_chars = 400.0;
        let errors = 5.0;
        let time = time::Duration::from_secs(120);
        let wpm = calculate_wpm(typed_chars, errors, time);
        assert_eq!(wpm, 37);
    }
}
