use ansi_escapes;
use anyhow::Result;
use console::Term;
use std::env;
use std::fs;
use std::io;
use std::io::Write;
use std::process::Command;
use std::{thread, time};
use todors;

pub fn start_interactive() -> Result<()> {
    let stdout = Term::buffered_stdout();
    let mut todo_lists: Vec<String> = Vec::new();
    let mut highlight_idx: usize = 0;

    for todo_list in fs::read_dir(todors::TodoList::get_path(todors::TodoList::default()))? {
        todo_lists.push(todors::TodoList::get_todolist_name(
            &todo_list
                .unwrap()
                .file_name()
                .into_string()
                .unwrap()
                .replace(".txt", ""),
        ));
    }

    if todo_lists.len() == 0 {
        println!("There are no todo lists to show, please make one!");
        let mut new_list = String::new();
        println!("Name of the todo list: ");
        io::stdin().read_line(&mut new_list)?;
        if new_list.eq("") {
            panic!("No input given, cannot make todo list");
        } else {
            todors::make_todo_list(&String::from(new_list.trim()))?;
            todo_lists.push(String::from(new_list.trim()));
        }
    }

    loop {
        println!("{}", ansi_escapes::EraseScreen);
        println!("{}", ansi_escapes::CursorTo::TopLeft);
        println!("{}", ansi_escapes::CursorHide);

        let item = &todo_lists[highlight_idx];
        todo_lists[highlight_idx] = String::from("\x1b[30;47m") + &item + &String::from("\x1b[0m");

        if todo_lists.len() >= 3 {
            if highlight_idx != todo_lists.len() - 1 && highlight_idx != 0 {
                let prev_item = &todo_lists[highlight_idx - 1];
                let next_item = &todo_lists[highlight_idx + 1];
                if prev_item.contains("\x1b[30:47m") {
                    let mod_prev_item = prev_item.replace("\x1b[30;47m", "");
                    todo_lists[highlight_idx - 1] = String::from(mod_prev_item);
                } else if next_item.contains("\x1b[30;47m") {
                    let mod_next_item = next_item.replace("\x1b[30;47m", "");
                    todo_lists[highlight_idx + 1] = String::from(mod_next_item);
                }
            }
        }
        if highlight_idx > 0 && highlight_idx < todo_lists.len() {
            let prev_item = &todo_lists[highlight_idx - 1];
            let mod_prev_item = prev_item.replace("\x1b[30;47m", "");
            todo_lists[highlight_idx - 1] = String::from(mod_prev_item);
        } else if highlight_idx == 0 {
            if todo_lists.len() > 1 {
                if todo_lists[highlight_idx + 1].contains("\x1b[30;47m") {
                    let other_item = &todo_lists[highlight_idx + 1];
                    let mod_item = other_item.replace("\x1b[30;47m", "");
                    todo_lists[highlight_idx + 1] = String::from(mod_item);
                }
            }
        }

        todo_lists.iter().for_each(|list| println!("{}", list));

        let mut editor_cmd = Command::new(env::var("EDITOR").unwrap());

        if let Ok(character) = stdout.read_char() {
            if character.eq(&'i') {
                editor_cmd
                    .arg(
                        todors::TodoList::get_path(todors::TodoList::default())
                            + &String::from("todo-")
                            + &todo_lists[highlight_idx]
                                .replace("\x1b[30;47m", "")
                                .replace("\x1b[0m", "")
                            + &String::from(".txt"),
                    )
                    .status()?;
                continue;
            } else if character.eq(&'l') {
                editor_cmd
                    .arg(
                        todors::TodoList::get_path(todors::TodoList::default())
                            + &String::from("todo-")
                            + &todo_lists[highlight_idx]
                                .replace("\x1b[30;47m", "")
                                .replace("\x1b[0m", "")
                            + &String::from(".txt"),
                    )
                    .status()?;
                continue;
            } else if character.eq(&'m') {
                let mut filename = String::new();
                println!("Name of todo list: ");
                io::stdin().read_line(&mut filename)?;
                todors::make_todo_list(&filename.strip_suffix("\n").unwrap().to_string()).unwrap();
                todo_lists.push(String::from(filename.strip_suffix("\n").unwrap()));
                continue;
            } else if character.eq(&'a') {
                let mut item = String::new();
                let mut date = String::new();
                println!("Name of item to add: ");
                io::stdin().read_line(&mut item)?;
                println!();
                print!("Due date of {}: ", &item.strip_suffix("\n").unwrap());
                io::stdout().flush()?;
                io::stdin().read_line(&mut date)?;
                todors::make_todo_item(
                    &todo_lists[highlight_idx]
                        .replace("\x1b[30;47m", "")
                        .replace("\x1b[0m", ""),
                    String::from(&item.strip_suffix("\n").unwrap().to_string()),
                    String::from(&date),
                )
                .unwrap();
                continue;
            } else if character.eq(&'d') {
                if todo_lists[highlight_idx].contains("-") {
                    let new_todo_lists = todo_lists
                        .iter()
                        .filter(|item| !item.contains("-"))
                        .map(|item| item.replace("\x1b[30;47m", "").replace("\x1b[0m", ""))
                        .collect::<Vec<String>>();

                    for list in &new_todo_lists {
                        let list_content = todors::read_file(
                            &(String::from("todo-") + &list + &String::from(".txt")),
                        )?;

                        for item in list_content.split("\n") {
                            if list_content.contains(&item) {
                                todors::delete_todo_item(&list, String::from(item))?;
                            }
                        }
                    }
                    todo_lists.remove(highlight_idx);
                } else {
                    todors::delete_todo_list(
                        &todo_lists[highlight_idx]
                            .replace("\x1b[30;47m", "")
                            .replace("\x1b[0m", ""),
                    )
                    .unwrap();

                    todo_lists.remove(highlight_idx);
                }

                if highlight_idx == todo_lists.len() {
                    highlight_idx = highlight_idx - 1;
                }
                continue;
            } else if character.eq(&'D') {
                let mut item: String = String::new();
                let mut yes_or_no: String = String::new();
                println!("item to remove: ");
                io::stdin().read_line(&mut item)?;
                io::stdout().flush()?;
                println!("Are you sure you want to remove {} (Y/n): ", item.trim());
                io::stdin().read_line(&mut yes_or_no)?;
                match yes_or_no.trim() {
                    "Y" => todors::delete_todo_item(
                        &todo_lists[highlight_idx]
                            .replace("\x1b[30;47m", "")
                            .replace("\x1b[0m", ""),
                        String::from(item.trim()),
                    )
                    .unwrap(),
                    "n" => continue,
                    _ => todors::delete_todo_item(
                        &todo_lists[highlight_idx]
                            .replace("\x1b[30;47m", "")
                            .replace("\x1b[0m", ""),
                        String::from(item.trim()),
                    )
                    .unwrap(),
                }
            } else if character.eq(&'e') {
                let list_content = todors::read_file(
                    &(String::from("todo-")
                        + &todo_lists[highlight_idx]
                            .replace("\x1b[30;47m", "")
                            .replace("\x1b[0m", "")
                        + &String::from(".txt")),
                )?;

                if list_content.is_empty() {
                    println!("This list is empty!");
                    thread::sleep(time::Duration::from_secs(2));
                    continue;
                }

                if highlight_idx != todo_lists.len() - 1 {
                    if todo_lists[highlight_idx + 1].contains("-") {
                        println!("This list is already expanded!");
                        thread::sleep(time::Duration::from_secs(2));
                        continue;
                    }
                }

                let cur_todo_content = fs::read_to_string(
                    String::from(todors::TodoList::get_path(todors::TodoList::default()))
                        + &String::from("todo-")
                        + &todo_lists[highlight_idx]
                            .replace("\x1b[30;47m", "")
                            .replace("\x1b[0m", "")
                        + &String::from(".txt"),
                )?;

                for item in cur_todo_content.trim().split("\n") {
                    todo_lists.insert(highlight_idx + 1, String::from("- ") + &String::from(item));
                }

                continue;
            }

            match character {
                'j' => highlight_idx += 1,
                'k' => highlight_idx -= 1,
                'q' => break,
                _ => continue,
            }
        }
    }
    Ok(())
}
