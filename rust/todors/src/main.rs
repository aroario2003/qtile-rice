use anyhow::Result;
use clap::{Arg, ArgAction, Command};
use todors;

mod util;

fn main() -> Result<()> {
    let flags = Command::new("todors")
        .about("A todo list cli and tui application written in rust")
        .version("0.0.1")
        .arg_required_else_help(true)
        .author("Alejandro Rosario")
        .arg(
            Arg::new("start_interactive")
                .short('s')
                .long("start-interactive")
                .help("Start the todors tui")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("make_todo_list")
                .short('m')
                .long("make-todolist")
                .help("make a new todo list")
                .value_names(&["todo-list"])
                .action(ArgAction::Set)
                .num_args(1),
        )
        .arg(
            Arg::new("delete_todo_list")
                .short('d')
                .long("delete-todolist")
                .help("delete a todo list completely")
                .value_names(&["todo-list"])
                .action(ArgAction::Set)
                .num_args(1),
        )
        .arg(
            Arg::new("make_todo_item")
                .short('i')
                .long("make-todoitem")
                .help("make a new todo item in the specified list")
                .value_names(&["todo-list", "todo-item", "due-date"])
                .action(ArgAction::Set)
                .num_args(3),
        )
        .arg(
            Arg::new("delete_todo_item")
                .short('D')
                .long("delete-todoitem")
                .help("delete a todo item in a specified todo list")
                .value_names(&["todo-list", "todo-item"])
                .action(ArgAction::Set)
                .num_args(2),
        )
        .arg(
            Arg::new("modify_date")
                .short('M')
                .long("modify-date")
                .help("modify the due date for a specified todo item")
                .value_names(&["todo-list", "todo-item", "new-date"])
                .action(ArgAction::Set)
                .num_args(3),
        )
        .arg(
            Arg::new("check_if_due")
                .short('c')
                .long("check-due-date")
                .help("check if an item is due on the current date")
                .value_names(&["todo-list", "todo-item"])
                .action(ArgAction::Set)
                .num_args(2),
        )
        .arg(
            Arg::new("mark_as_done")
                .short('C')
                .long("mark-as-done")
                .help("Mark the specified item as done in the todo list")
                .value_names(&["todo-list", "todo-item"])
                .action(ArgAction::Set)
                .num_args(2),
        )
        .get_matches();

    if flags.get_flag("start_interactive") {
        util::start_interactive()?
    } else if flags.contains_id("make_todo_list") {
        let todo_args = flags.get_raw("make_todo_list").unwrap();
        let flag_args = todo_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::make_todo_list(&String::from(flag_args[0]))?;
    } else if flags.contains_id("delete_todo_list") {
        let todo_args = flags.get_raw("delete_todo_list").unwrap();
        let flag_args = todo_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::delete_todo_list(&String::from(flag_args[0]))?;
    } else if flags.contains_id("make_todo_item") {
        let item_args = flags.get_raw("make_todo_item").unwrap();
        let flag_args = item_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::make_todo_item(
            &String::from(flag_args[0]),
            String::from(flag_args[1]),
            String::from(flag_args[2]),
        )?;
    } else if flags.contains_id("delete_todo_item") {
        let item_args = flags.get_raw("delete_todo_item").unwrap();
        let flag_args = item_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::delete_todo_item(&String::from(flag_args[0]), String::from(flag_args[1]))?;
    } else if flags.contains_id("modify_date") {
        let item_args = flags.get_raw("modify_date").unwrap();
        let flag_args = item_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::modify_date(
            &String::from(flag_args[0]),
            String::from(flag_args[1]),
            String::from(flag_args[2]),
        )?;
    } else if flags.contains_id("check_if_due") {
        let item_args = flags.get_raw("check_if_due").unwrap();
        let flag_args = item_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::check_if_item_due(&String::from(flag_args[0]), String::from(flag_args[1]))?;
    } else if flags.contains_id("mark_as_done") {
        let done_args = flags.get_raw("mark_as_done").unwrap();
        let flag_args = done_args
            .into_iter()
            .map(|x| x.to_str().unwrap())
            .collect::<Vec<&str>>();

        todors::mark_item_as_done(&String::from(flag_args[0]), String::from(flag_args[1]))?;
    }
    Ok(())
}
