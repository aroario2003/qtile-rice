use anyhow::Result;
use chrono::Local;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};
use std::env::var;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::Path;


#[derive(Debug)]
pub struct TodoList {
    pub path: String,
    pub filename: String,
}

impl Default for TodoList {
    fn default() -> Self {
        let list = TodoList {
            path: String::from(var("HOME").unwrap()) + &String::from("/.local/share/todors/"),
            filename: String::from("todo-default.txt"),
        };
        list
    }
}

impl TodoList {
    pub fn get_path(self) -> String {
        let path = self.path;
        if !Path::exists(Path::new(&path)) {
            fs::create_dir(&path).unwrap();
        }
        path
    }

    pub fn get_todolist_name(filename: &String) -> String {
        let filename_parts = filename.split("-");
        let parts: Vec<&str> = filename_parts.collect();

        String::from(parts[1])
    }

    //This function is only used in the test module and therefore
    //suppressing the warning.
    #[allow(dead_code)]
    fn exists(&self) -> bool {
        let todo_list = self.path.clone() + &self.filename;
        if Path::exists(Path::new(&todo_list)) {
            return true;
        }
        false
    }
}

#[derive(Debug)]
pub struct TodoItem {
    pub value: String,
    pub date: String,
    pub done: String,
}

impl Default for TodoItem {
    fn default() -> Self {
        let item = TodoItem {
            value: String::from("unknown"),
            date: String::from("N/A"),
            done: String::from(" "),
        };
        item
    }
}

impl TodoItem {
    fn to_str(&self) -> String {
        let item = TodoItem {
            value: String::from(&self.value),
            date: String::from(&self.date),
            done: String::from(&self.done),
        };
        String::from(&item.value)
            + &String::from("     ")
            + &String::from(&item.date)
            + &String::from("   ")
            + &String::from(&item.done)
    }

    fn check_if_due(&self) -> bool {
        let due_date = &self.date;
        let datetime = Local::now();
        let cur_date = datetime.date_naive().to_string();

        if due_date.eq(&cur_date) {
            return true;
        }
        false
    }
}

pub fn read_file(filename: &String) -> Result<String> {
    let list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from(filename),
    };

    let mut todo_list: File = File::open(list.path + &list.filename)?;

    let mut contents: String = String::new();
    todo_list.read_to_string(&mut contents)?;

    Ok(contents)
}

pub fn make_todo_list(filename: &String) -> Result<String> {
    let list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from(filename),
    };

    File::create(list.path + &String::from("todo-") + &list.filename + &String::from(".txt"))?;
    Ok(list.filename)
}

pub fn delete_todo_list(filename: &String) -> Result<()> {
    fs::remove_file(
        TodoList::get_path(TodoList::default())
            + &String::from("todo-")
            + &String::from(filename)
            + &String::from(".txt"),
    )?;
    Ok(())
}

pub fn make_todo_item(filename: &String, item: String, date: String) -> Result<()> {
    let item = TodoItem {
        value: item,
        date: date,
        done: TodoItem::default().done,
    };

    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .open(
            TodoList::get_path(TodoList::default())
                + &String::from("todo-")
                + &String::from(filename)
                + &String::from(".txt"),
        )
        .unwrap();

    if let Err(e) = writeln!(file, "{}", item.to_str()) {
        eprintln!("Couldn't add item {}", e);
    }

    Ok(())
}

pub fn delete_todo_item(filename: &String, item_name: String) -> Result<()> {
    let list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from("todo-") + &String::from(filename) + &String::from(".txt"),
    };

    let todo_content: String = read_file(&list.filename).unwrap();

    let split = todo_content.lines();
    let mut contents_vec: Vec<&str> = split.collect();

    let idx_of_item: usize = contents_vec
        .iter()
        .position(|item| item.contains(&item_name))
        .unwrap();

    contents_vec.remove(idx_of_item);

    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .open(list.path.to_owned() + &list.filename)
        .unwrap();

    if contents_vec.is_empty() {
        fs::write(list.path + &list.filename, "")?;
    } else {
        fs::write(list.path.to_owned() + &list.filename, "")?;
        for s in contents_vec {
            if !s.eq("") {
                if let Err(e) = writeln!(file, "{}", s) {
                    eprintln!("Couldn't change list {}", e);
                }
            }
        }
    }
    Ok(())
}

pub fn modify_date(filename: &String, item_name: String, date: String) -> Result<String> {
    let list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from("todo-") + &String::from(filename) + &String::from(".txt"),
    };

    let todo_content: String = read_file(&list.filename).unwrap();

    let split = todo_content.lines();
    let mut contents_vec: Vec<&str> = split.collect();

    let idx_of_item: usize = contents_vec
        .iter()
        .position(|item| item.contains(&item_name))
        .unwrap();

    let full_item: String = String::from(contents_vec[idx_of_item]);
    let item_parts = full_item.split("     ").collect::<Vec<&str>>();

    let (mut due_date, done) = item_parts[1].split_once("   ").unwrap();
    due_date = &date;

    let new_item = String::from(item_parts[0])
        + &String::from("     ")
        + &String::from(due_date)
        + &String::from("   ")
        + &String::from(done);

    contents_vec[idx_of_item] = &new_item;

    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .open(list.path.to_owned() + &list.filename)
        .unwrap();

    fs::write(list.path.to_owned() + &list.filename, "")?;

    for s in contents_vec {
        if !s.eq("") {
            if let Err(e) = writeln!(file, "{}", s) {
                eprintln!("Couldn't change date {}", e);
            }
        }
    }

    Ok(new_item)
}

pub fn check_if_item_due(filename: &String, item_name: String) -> Result<bool> {
    let list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from("todo-") + &String::from(filename) + &String::from(".txt"),
    };

    let todo_content: String = read_file(&list.filename).unwrap();

    let split = todo_content.lines();
    let contents_vec: Vec<&str> = split.collect();

    let idx_of_item: usize = contents_vec
        .iter()
        .position(|item| item.contains(&item_name))
        .unwrap();

    let todo_item = contents_vec[idx_of_item];
    let todo_item_parts = todo_item.split("     ").collect::<Vec<&str>>();
    let (due_date, done) = todo_item_parts[1].split_once("   ").unwrap();

    let item = TodoItem {
        value: String::from(todo_item_parts[0]),
        date: String::from(due_date),
        done: String::from(done),
    };

    let due: bool = item.check_if_due();

    if due {
        return Ok(true);
    }
    Ok(false)
}

fn send_sms_if_item_due(filename: &String, item_name: String) -> Result<()> {
    let todo_list = TodoList {
        path: TodoList::get_path(TodoList::default()),
        filename: String::from("todo-") + &filename + &String::from(".txt"),
    };

    let list_content = read_file(&todo_list.filename)?;

    let item = list_content
        .lines()
        .filter(|item| item.contains(&item_name))
        .collect::<String>();

    let item_parts = item
        .split("     ")
        .map(|elem| elem.to_string())
        .collect::<Vec<String>>();

    if item_parts.len() < 2 {
        return Ok(());
    }

    let todo_item = TodoItem {
        value: item_parts[0].clone(),
        date: item_parts[1].clone(),
        done: TodoItem::default().done,
    };

    if todo_item.check_if_due() {
        let message = Message::builder()
            .from("<borkbork1031@gmail.com>".parse().unwrap())
            .to("<7032292325@vtext.com>".parse().unwrap())
            .body(todo_item.value + &String::from(" is due today"))
            .unwrap();

        let credentials = Credentials::new(
            "borkbork1031@gmail.com".to_string(),
            "oxuyvlvvciuplphm".to_string(),
        );

        let gmail_con = SmtpTransport::relay("smtp.gmail.com")
            .unwrap()
            .port(465)
            .credentials(credentials)
            .build();

        match gmail_con.send(&message) {
            Ok(_) => println!("The message has been sent"),
            Err(e) => println!("The message failed to send: {:?}", e),
        }
    }
    Ok(())
}

pub fn due_date_checker(path: String) -> Result<()> {
    let todo_lists = fs::read_dir(&path)?;
    let todo_list_filenames = todo_lists
        .map(|list| list.unwrap().file_name().into_string().unwrap())
        .collect::<Vec<String>>();

    let todo_list_names = todo_list_filenames
        .iter()
        .map(|file| file.split("-"))
        .flatten()
        .filter(|item| item.contains(".txt"))
        .map(|item| item.split("."))
        .flatten()
        .filter(|item| !item.contains("txt"))
        .collect::<Vec<&str>>();

    for list in todo_list_names {
        let list_content = read_file(&(String::from("todo-") + &list + &String::from(".txt")))?;

        if !list_content.is_empty() {
            for item in list_content.lines() {
                if item.is_empty() {
                    continue;
                }

                let item_iter = item.split("     ");
                let item_parts = item_iter
                    .map(|elem| elem.to_string())
                    .collect::<Vec<String>>();

                send_sms_if_item_due(&String::from(list), item_parts[0].clone())?;
            }
        } else {
            continue;
        }
    }
    Ok(())
}

pub fn mark_item_as_done(filename: &String, item_name: String) -> Result<()> {
    let list_content =
        read_file(&(String::from("todo-") + &String::from(filename) + &String::from(".txt")))?;
    let mut items = list_content.split("\n").collect::<Vec<&str>>();

    let idx_of_item = items
        .iter()
        .position(|item| item.contains(&item_name))
        .unwrap();
    let done_item = items[idx_of_item];

    items.remove(idx_of_item);

    let item_parts = done_item.split("     ").collect::<Vec<&str>>();
    let (due_date, mut done) = item_parts[1].split_once("   ").unwrap();

    if done.eq(" ") {
        println!("The item has already been marked as done");
    } else if done.eq(" ") {
        done = " ";
        let new_item = String::from(item_parts[0])
            + &String::from("     ")
            + &String::from(due_date)
            + &String::from("   ")
            + &String::from(done);

        items[idx_of_item] = &new_item;

        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(
                TodoList::get_path(TodoList::default())
                    + &String::from("todo-")
                    + &String::from(filename)
                    + &String::from(".txt"),
            )
            .unwrap();

        fs::write(
            TodoList::get_path(TodoList::default())
                + &String::from("todo-")
                + &String::from(filename)
                + &String::from(".txt"),
            "",
        )?;

        for item in items {
            if !item.eq("") {
                if let Err(e) = writeln!(file, "{}", item) {
                    eprintln!("Couldn't change date {}", e);
                }
            }
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn check_todo_list_exists() {
        let list = TodoList {
            path: TodoList::get_path(TodoList::default()),
            filename: String::from("test"),
        };

        if TodoList::exists(&list) {
            delete_todo_list(&String::from(list.filename)).expect("could not delete todo list");
        }

        make_todo_list(&String::from("test")).expect("could not make todo list");
        assert_eq!(
            File::open(var("HOME").unwrap() + "/.local/share/todors/todo-test.txt").is_ok(),
            true
        );
    }

    #[test]
    fn check_todo_list_deleted() {
        let list = TodoList {
            path: TodoList::get_path(TodoList::default()),
            filename: String::from("test"),
        };

        if TodoList::exists(&list) {
            delete_todo_list(&String::from(list.filename)).expect("could not delete todo list");
        }

        make_todo_list(&String::from("test")).expect("could not make todo list");
        delete_todo_list(&String::from("test")).expect("could not delete the todo list");
        assert_eq!(
            File::open(var("HOME").unwrap() + "/.local/share/todors/todo-test.txt").is_ok(),
            false
        );
    }

    #[test]
    fn check_item_due() {
        let list = TodoList {
            path: TodoList::get_path(TodoList::default()),
            filename: String::from("test"),
        };

        if TodoList::exists(&list) {
            delete_todo_list(&String::from(list.filename)).expect("could not delete todo list");
        }

        make_todo_list(&String::from("test")).expect("Could not create the todo list");
        make_todo_item(
            &String::from("test"),
            String::from("test item1"),
            String::from(Local::now().date_naive().to_string()),
        )
        .expect("Could not create the todo item");
        assert_eq!(
            check_if_item_due(&String::from("test"), String::from("test item1")).unwrap(),
            true
        );
    }

    #[test]
    fn check_due_date_changed() {
        let list = TodoList {
            path: TodoList::get_path(TodoList::default()),
            filename: String::from("test"),
        };

        if TodoList::exists(&list) {
            delete_todo_list(&String::from(list.filename)).expect("could not delete todo list");
        }

        let cur_date = Local::now().date_naive().to_string();
        make_todo_list(&String::from("test")).expect("could not make the todo list");
        make_todo_item(
            &String::from("test"),
            String::from("test item"),
            String::from(cur_date),
        )
        .expect("Unable to create todo item");
        let new_item = modify_date(
            &String::from("test"),
            String::from("test item"),
            String::from("2023-12-31"),
        )
        .expect("could not modify due date");
        assert_eq!(new_item, "test item     2023-12-31    ");
    }
}
