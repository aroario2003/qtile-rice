from typing import List, Text  # noqa: F401
import os
import re
import socket
import json
import psutil
import subprocess
from libqtile import qtile
from libqtile import bar, layout
from libqtile.config import Click, Drag, Group, KeyChord, ScratchPad, DropDown, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile import hook
from Xlib import display as xdisplay
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration 

mod = "mod4"
terminal = "alacritty"
wallpaper="nitrogen"
files="alacritty -e ranger"

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

@hook.subscribe.client_new
def _swallow(window):
    pid = window.window.get_net_wm_pid()
    ppid = psutil.Process(pid).ppid()
    cpids = {c.window.get_net_wm_pid(): wid for wid, c in window.qtile.windows_map.items()}
    for i in range(5):
        if not ppid:
            return
        if ppid in cpids:
            parent = window.qtile.windows_map.get(cpids[ppid])
            parent.minimized = True
            window.parent = parent
            return
        ppid = psutil.Process(ppid).ppid()

@hook.subscribe.client_killed
def _unswallow(window):
    if hasattr(window, 'parent'):
        window.parent.minimized = False

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, ], "l", lazy.layout.shrink(), desc="Grow window to the left"),
    Key([mod, ], "h", lazy.layout.grow(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "r", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod, ], "comma", lazy.prev_screen(), desc="focus to monitor"),
    Key([mod, ], "period", lazy.next_screen(), desc="focus to monitor"),
    Key([mod, "shift"], "period", lazy.function(window_to_previous_screen), desc="focus to monitor"),
    Key([mod, "shift"], "comma", lazy.function(window_to_next_screen), desc="focus to monitor"),
    Key([mod, "shift"], "f", lazy.window.toggle_fullscreen(), desc="fullscreen"),
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "c", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    KeyChord([mod], "f", [
        Key([], "p", lazy.spawn("thunar"), desc="filemanager"),
        Key([], "r", lazy.spawn(terminal + " -e ranger"), desc="terminal filemanager"),
        Key([], "f", lazy.window.toggle_floating(), desc="toggle floating"),
    ]),
]

groups= [
    Group("1",
          label=""
          ),

    Group("2",
          label="",
          matches = [Match(wm_class="Brave-browser")],
          ),

    Group("3",
          label=""
          ),

    Group("4",
          label="",
          matches = [Match(wm_class="Emacs")],
          ),

    Group("5",
          label=""
          ),

    Group("6",
          label=""
          ),

    Group("7",
          label=""
          ),

    Group("8",
          label=""
          ),

    Group("9",
          label=""
          ),
]

for i in groups:
    keys.extend([

        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

class Scratchpad(object):

	def init_scratchpad(self):

		height =				0.50
		y_position =			0.25
		warp_pointer =			False
		on_focus_lost_hide =	True
		opacity =				1

		return [
			ScratchPad("SPD",
				dropdowns = [
					DropDown("term",
						terminal,
						opacity = opacity,
						y = y_position,
                        x = 0.25,
						height = height,
                        width = 0.50,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer),
                    DropDown("wall",
                        wallpaper,
						opacity = opacity,
						y = y_position,
                        width = 0.50,
                        x = 0.25,
						height = height,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer),
                    DropDown("filemanager",
                        files,
						opacity = opacity,
						y = y_position,
                        width = 0.50,
                        x = 0.25,
						height = height,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer)

				]
			),
		]

class DropDown_Keys(object):

	def init_dropdown_keybindings(self):

		mod =	"mod4"
		alt =	"mod1"
		altgr =	"mod5"

		return [
         KeyChord([mod], "s", [
			Key([], "t",
				lazy.group["SPD"].dropdown_toggle("term")),
			Key([], "n",
				lazy.group["SPD"].dropdown_toggle("wall")),
            Key([], "f",
              lazy.group["SPD"].dropdown_toggle("filemanager"))
		]),
    ]

groups += Scratchpad().init_scratchpad()
keys += DropDown_Keys().init_dropdown_keybindings()

everforest = {
    "background": "#2b3339",
    "foreground": "#d3c6aa",
    "border_focus": "#7fbbb3",
    "border_normal": "#2b3339",
    "workspace_focused": "#7fbbb3",
    "workspace_occupied": "#d699b6",
    "workspace_empty": "#d3c6aa",
    "widget_background": "#323C41",
    "black": "#2b3339",
    "red": "#e67e80",
    "yellow": "#ddbc7f",
    "cyan": "#83c092",
    "pink": "#d699b6",
    "blue": "#7fbbb3",
    "green": "#A3BE8C",
    "white": "#d3c6aa"
}

gruvbox = {
    "background": "#282828",
    "foreground": "#ebdbb2",
    "border_focus": "#458588",
    "border_normal": "#282828",
    "workspace_focused": "#458588",
    "workspace_occupied": "#b16286",
    "workspace_empty": "#ebdbb2",
    "widget_background": "#3c3836",
    "black": "#282828",
    "red": "#cc241d",
    "yellow": "#d79921",
    "cyan": "#689d6a",
    "pink": "#b16286",
    "blue": "#458588",
    "green": "#98971a",
    "white": "#ebdbb2"
}

nord = {
    "background": "#2E3440",
    "foreground": "#d8dee9",
    "border_focus": "#81a1c1",
    "border_normal": "#2e3440",
    "workspace_focused": "#81a1c1",
    "workspace_occupied": "#b48ead",
    "workspace_empty": "#d8dee9",
    "widget_background": "#3b4252",
    "black": "#2e3440",
    "red": "#bf616a",
    "yellow": "#ebcb8b",
    "cyan": "#88c0d0",
    "pink": "#b48ead",
    "blue": "#81a1c1",
    "green": "#a3be8c",
    "white": "#e5e9f0"
}

tokyonight = {
    "background": "#1a1b26",
    "foreground": "#a9b1d6",
    "border_focus": "#7aa2f7",
    "border_normal": "#1a1b26",
    "workspace_focused": "#7aa2f7",
    "workspace_occupied": "#ad8ee6",
    "workspace_empty": "#787c99",
    "widget_background": "#292e42",
    "black": "#1a1b26",
    "red": "#f7768e",
    "yellow": "#e0af68",
    "cyan": "#449dab",
    "pink": "#ad8ee6",
    "blue": "#7aa2f7",
    "green": "#9ece6a",
    "white": "#787c99"
}

articblush = {
    "background": "#040c16",
    "foreground": "#cce9ea",
    "border_focus": "#bdd6f4",
    "border_normal": "#040c16",
    "workspace_focused": "#bdd6f4",
    "workspace_occupied": "#f9ecf7",
    "workspace_empty": "#edf7f8",
    "widget_background": "#15171a",
    "black": "#040c16",
    "red": "#ff7377",
    "green": "#aaf0c1",
    "yellow": "#eadd94",
    "blue": "#bdd6f4",
    "pink": "#f9ecf7",
    "cyan": "#b3ffff",
    "white": "#edf7f8",
}

decay = {
    "background": "#101419",
    "foreground": "#b6beca",
    "border_focus": "#70a5eb",
    "border_normal": "#101419",
    "workspace_focused": "#70a5eb",
    "workspace_occupied": "#c68aee",
    "workspace_empty": "#384148",
    "widget_background": "#2b2d36",
    "black": "#101419",
    "red": "#e05f65",
    "green": "#78dba9",
    "yellow": "#f1cf8a",
    "blue": "#70a5eb",
    "pink": "#c68aee",
    "cyan": "#74bee9",
    "white": "#dee1e6",
}

dracula = {
    "background": "#282a36",
    "foreground": "#f8f8f2",
    "border_focus": "#bd93f9",
    "border_normal": "#282a36",
    "workspace_focused": "#bd93f9",
    "workspace_occupied": "#ff79c6",
    "workspace_empty": "#3e4153",
    "widget_background": "#3e4153",
    "black": "#282a36",
    "red": "#ff5555",
    "green": "#50fa7b",
    "yellow": "#f1fa8c",
    "blue": "#bd93f9",
    "pink": "#ff79c6",
    "cyan": "#8be9fd",
    "white": "#f8f8f2",   
}

colorscheme = tokyonight

layouts = [

    #layout.Columns(border_focus_stack=['#83c092', '#8f3d3d'], border_width=4),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
     layout.MonadTall(border_focus=colorscheme["border_focus"], margin=15, border_normal=colorscheme["border_normal"]),
     layout.Max(),
     layout.Floating(border_focus=colorscheme["border_focus"], border_width=2, border_normal=colorscheme["border_normal"]),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Iosevka Nerd Font',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

def run_eww_power():
    qtile.cmd_spawn('eww open-many powermenu messagewin')

screens= [
 Screen(
       top=bar.Bar(
              [
                widget.TextBox(text="       ", font="Fira Code Nerd Font", padding=0, background=colorscheme["background"], foreground=colorscheme["pink"], mouse_callbacks={'Button1': run_eww_power},
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=3)],),
                widget.GroupBox(
                    active=colorscheme["workspace_occupied"],
                    inactive=colorscheme["workspace_empty"],
                    borderwidth=2,
                    border='bd93fd',
                    highlight_method="line",
                    highlight_color=colorscheme["black"],
                    block_highlight_text_color=colorscheme["workspace_focused"],
                    other_current_screen_border=colorscheme["black"],
                    this_current_screen_border=colorscheme["black"],
                ),
                widget.Prompt(),
                widget.WindowName(foreground=colorscheme["foreground"], fontsize=14, padding=10),
                widget.PulseVolume(background=colorscheme["background"], foreground=colorscheme["red"], get_volume_command="pamixer --get-volume", fmt='    {}  ',
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=0)],),
                widget.Memory(format='{MemUsed: .0f}MB', foreground=colorscheme["cyan"], background=colorscheme["background"], update_interval=5.0, fmt='   {}  ',
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=7)],),
                widget.CPU(format='{load_percent}%', foreground=colorscheme["blue"], background=colorscheme["background"], fmt='    {}  ',
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=0)],),
                widget.DF(visible_on_warn=False, format='{uf}GB', foreground=colorscheme["pink"], background=colorscheme["background"], fmt='    {}  ',
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=7)],),
                widget.ThermalSensor(foreground=colorscheme["yellow"],background=colorscheme["background"], padding=9, fmt='  {} ', 
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=0)],),
                widget.Battery(format='{percent:2.0%}', foreground=colorscheme["green"], background=colorscheme["background"], fmt='   {}  ', 
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=7)],),
                widget.Clock(format='    %b %d, %Y  ', foreground=colorscheme["red"], background=colorscheme["background"], 
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=0)],),
                widget.Clock(format='   %l:%M%p  ', foreground=colorscheme["cyan"], background=colorscheme["background"], 
                decorations=[RectDecoration(colour=colorscheme["widget_background"], filled=True, radius=10, padding_y=None, padding_x=7)],),
                ],
            22,
            background=colorscheme["background"]
            ),
       ),
 ]

def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors

num_monitors = get_num_monitors()

if num_monitors > 1:
    for m in range(num_monitors - 1):
        screens.append(
           Screen(
              top=bar.Bar(
              [
                widget.GroupBox(
                    active='#7fbbb3',
                    inactive='#d699b6',
                    borderwidth=2,
                    border='bd93fd',
                    highlight_method="line",
                    highlight_color='#393939',
                    other_current_screen_border='#7fbbb3',
                    this_current_screen_border='#7fbbb3',

                ),
                widget.Prompt(),
                widget.WindowName(foreground='#7fbbb3'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#e67e80', padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=11, foreground='#000000', background='#e67e80', padding=5),
                widget.Volume(foreground='#000000', background='#e67e80', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#83c092', background = "#e67e80", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=14, foreground='#000000', background='#83c092', padding=3),
                widget.Memory(format='{MemUsed: .0f}MB ', foreground='#000000', background='#83c092', update_interval=5.0),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#7fbbb3', background = "#83c092", padding=0, update_interval=60 ),
                widget.TextBox(text=" ", update_interval=5.0, font="Font Awesome 5 Free", foreground='#000000', background='#7fbbb3'),
                widget.CPU(format='{load_percent}% ', foreground='#000000', background='#7fbbb3'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#d699b6', background = "#7fbbb3", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#d699b6'),
                widget.DF(visible_on_warn=False, format='{uf}GB ', foreground='#000000', background='#d699b6'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#ddbc7f',background = "#d699b6", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=12, foreground='#000000', background='#ddbc7f'),
                widget.ThermalSensor(foreground='#000000',background='#ddbc7f', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#A3BE8C',background = "#ddbc7f", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#A3BE8C'),
                widget.Battery(format='{percent:2.0%} ', foreground='#000000', background='#A3BE8C'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#e67e80',background = "#A3BE8C", padding=0, update_interval=60 ),
                widget.Clock(format='   %B %d, %Y ', foreground='#000000', background='#e67e80'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#83c092', background = "#e67e80", padding=0, update_interval=60 ),
                widget.Clock(format='  %l:%M%p ', foreground='#000000', background='#83c092'),
                widget.CurrentLayoutIcon(custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")], scale=0.8, background='#7fbbb3'),
                                          ],
            18,
            background='#00000000'
            ),
       )
 )

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus=colorscheme["border_focus"],
    border_width=2,
    float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  
    Match(wm_class='makebranch'),  
    Match(wm_class='maketag'), 
    Match(wm_class='Nitrogen'),
    Match(wm_class='Lxappearance'),
    Match(wm_class='ssh-askpass'),  
    Match(title='branchdialog'),  
    Match(title='pinentry'),  
    Match(wm_class='Tk'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = True

@hook.subscribe.setgroup
def setgroup():
    for i in range(0, 9):
        qtile.groups[i].label = ""
    qtile.current_group.label = "󰮯"

@hook.subscribe.startup_once
def start_once():
  home = os.path.expanduser('~')
  subprocess.call([home + '/.config/qtile/autostart.sh'])

wmname = "Qtile"
