from typing import List  # noqa: F401
import os
import re
import socket
import json
import psutil
import subprocess
from libqtile import qtile
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, KeyChord, ScratchPad, DropDown, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile import hook
from Xlib import display as xdisplay

mod = "mod4"
terminal = "alacritty"
wallpaper="nitrogen"
files="kitty -e ranger"

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

@hook.subscribe.client_new
def _swallow(window):
    pid = window.window.get_net_wm_pid()
    ppid = psutil.Process(pid).ppid()
    cpids = {c.window.get_net_wm_pid(): wid for wid, c in window.qtile.windows_map.items()}
    for i in range(5):
        if not ppid:
            return
        if ppid in cpids:
            parent = window.qtile.windows_map.get(cpids[ppid])
            parent.minimized = True
            window.parent = parent
            return
        ppid = psutil.Process(ppid).ppid()

@hook.subscribe.client_killed
def _unswallow(window):
    if hasattr(window, 'parent'):
        window.parent.minimized = False

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
  #  Key([mod,        ], "b", lazy.spawn("brave"), desc="browser"),
    Key([mod, ], "l", lazy.layout.shrink(), desc="Grow window to the left"),
    Key([mod, ], "h", lazy.layout.grow(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "r", lazy.layout.normalize(), desc="Reset all window sizes"),
   # Key([mod, "shift"], "v", lazy.spawn("pavucontrol"), desc="volume"),
    Key([mod, ], "comma", lazy.prev_screen(), desc="focus to monitor"),
    Key([mod, ], "period", lazy.next_screen(), desc="focus to monitor"),
    Key([mod, "shift"], "period", lazy.function(window_to_previous_screen), desc="focus to monitor"),
    Key([mod, "shift"], "comma", lazy.function(window_to_next_screen), desc="focus to monitor"),
    Key([mod, "shift"], "f", lazy.window.toggle_fullscreen(), desc="fullscreen"),
   # Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -n -h 20"), desc="dmenu"),
   # Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
   # Key([mod, "shift"], "n", lazy.spawn("nitrogen"), desc="wallpaper"),
   # Key([mod, ], "v", lazy.spawn("virtualbox"), desc="virtual machine"),
   # Key([mod, "shift"], "d", lazy.spawn("discord"), desc="discord"),
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile"),
   # Key([mod], "p", lazy.spawn("flameshot screen -p /home/hacker/Pictures"), desc="screenshot"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
   # Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master toggle"), desc="Mute Volume"),
   # Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 1%-"), desc="Lower Volume"),
   # Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 1%+"), desc="Raise Volume"),
    Key([mod], "c", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
   # Key([mod], "Escape", lazy.spawn("powermenu"), desc="powermenu"),
    KeyChord([mod], "f", [
        Key([], "p", lazy.spawn("thunar"), desc="filemanager"),
        Key([], "r", lazy.spawn(terminal + " -e ranger"), desc="terminal filemanager"),
        Key([], "f", lazy.window.toggle_floating(), desc="toggle floating"),
    ]),
   # Key([mod, "shift" ], "s", lazy.spawn("rofi -show drun -hide-scrollbar -theme slate"), desc="launch programs"),
   # Key([mod], "n", lazy.spawn("kitty -e newsboat"), desc="open terminal with rss"),
]

groups= [
    Group("1",
          label=""
          ),

    Group("2",
          label="",
          matches = [Match(wm_class=["Brave-browser"])],
          ),

    Group("3",
          label=""
          ),

    Group("4",
          label=""
          ),

    Group("5",
          label=""
          ),

    Group("6",
          label=""
          ),

    Group("7",
          label=""
          ),

    Group("8",
          label=""
          ),

    Group("9",
          label=""
          ),
]
for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

class Scratchpad(object):

	def init_scratchpad(self):

		# Configuration
		height =				0.8000
		y_position =			0.0251
		warp_pointer =			False
		on_focus_lost_hide =	True
		opacity =				1

		return [
			ScratchPad("SPD",
				dropdowns = [
					# Drop down terminal with tmux session
					DropDown("term",
						terminal,
						opacity = opacity,
						y = y_position,
						height = height,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer),
                    DropDown("wall",
                        wallpaper,
						opacity = opacity,
						y = y_position,
						height = height,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer),
                    DropDown("filemanager",
                        files,
						opacity = opacity,
						y = y_position,
						height = height,
						on_focus_lost_hide = on_focus_lost_hide,
						warp_pointer = warp_pointer)

				]
			),
		]

class DropDown_Keys(object):

	##### DROPDOWNS KEYBINDINGS #####

	def init_dropdown_keybindings(self):

		# Key alias
		mod =	"mod4"
		alt =	"mod1"
		altgr =	"mod5"

		return [
         KeyChord([mod], "s", [
			Key([], "t",
				lazy.group["SPD"].dropdown_toggle("term")),
			Key([], "n",
				lazy.group["SPD"].dropdown_toggle("wall")),
            Key([], "f",
              lazy.group["SPD"].dropdown_toggle("filemanager"))
		]),
    ]

groups += Scratchpad().init_scratchpad()
keys += DropDown_Keys().init_dropdown_keybindings()

layouts = [

    #layout.Columns(border_focus_stack=['#88C0D0', '#8f3d3d'], border_width=4),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
     layout.MonadTall(border_focus='#81a1c1', margin=11, border_normal='#16161e'),
     layout.Max(),
     layout.Floating(border_focus='#81a1c1', border_width=2, border_normal='#16161e'),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Mononoki Nerd Font',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens= [
 Screen(
       top=bar.Bar(
              [
                widget.GroupBox(
                    active='#B48EAD',
                    inactive='#81a1c1',
                    borderwidth=2,
                    border='bd93fd',
                    highlight_method="line",
                    highlight_color='#393939',
                    other_current_screen_border='#81a1c1',
                    this_current_screen_border='#81a1c1',

                ),
                widget.Prompt(),
                widget.WindowName(foreground='#81a1c1'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#BF616a', padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=11, foreground='#000000', background='#BF616a', padding=5),
                widget.Volume(foreground='#000000', background='#BF616a', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#88c0d0', background = "#BF616a", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=14, foreground='#000000', background='#88c0d0', padding=3),
                widget.Memory(format='{MemUsed: .0f}MB ', foreground='#000000', background='#88c0d0', update_interval=5.0),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#81A1C1', background = "#88C0d0", padding=0, update_interval=60 ),
                widget.TextBox(text=" ", update_interval=5.0, font="Font Awesome 5 Free", foreground='#000000', background='#81A1C1'),
                widget.CPU(format='{load_percent}% ', foreground='#000000', background='#81a1c1'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#B48EAD', background = "#81a1c1", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#B48EAD'),
                widget.DF(visible_on_warn=False, format='{uf}GB ', foreground='#000000', background='#B48EAD'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#EBcB8B',background = "#B48EAD", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=12, foreground='#000000', background='#EBcB8B'),
                widget.ThermalSensor(foreground='#000000',background='#EBcB8B', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#A3BE8C',background = "#EBcB8B", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#A3BE8C'),
                widget.Battery(format='{percent:2.0%} ', foreground='#000000', background='#A3BE8C'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#BF616a',background = "#A3BE8C", padding=0, update_interval=60 ),
                widget.Clock(format='   %B %d, %Y ', foreground='#000000', background='#BF616a'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#88c0d0', background = "#BF616a", padding=0, update_interval=60 ),
                widget.Clock(format='  %l:%M%p ', foreground='#000000', background='#88c0d0'),
                widget.CurrentLayoutIcon(custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")], scale=0.8, background='#81a1c1'),
                                          ],
            20,
            background='#2e3440'
            ),
       ),
 ]

def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors

num_monitors = get_num_monitors()

if num_monitors > 1:
    for m in range(num_monitors - 1):
        screens.append(
           Screen(
              top=bar.Bar(
              [
                widget.GroupBox(
                    active='#B48EAD',
                    inactive='#81a1c1',
                    borderwidth=2,
                    border='bd93fd',
                    highlight_method="line",
                    highlight_color='#393939',
                    other_current_screen_border='#81a1c1',
                    this_current_screen_border='#81a1c1',

                ),
                widget.Prompt(),
                widget.WindowName(foreground='#81a1c1'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#BF616a', padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=11, foreground='#000000', background='#BF616a', padding=5),
                widget.Volume(foreground='#000000', background='#BF616a', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#88c0d0', background = "#BF616a", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Mononoki Nerd Font", fontsize=14, foreground='#000000', background='#88c0d0', padding=3),
                widget.Memory(format='{MemUsed: .0f}MB ', foreground='#000000', background='#88c0d0', update_interval=5.0),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#81A1C1', background = "#88C0d0", padding=0, update_interval=60 ),
                widget.TextBox(text=" ", update_interval=5.0, font="Font Awesome 5 Free", foreground='#000000', background='#81A1C1'),
                widget.CPU(format='{load_percent}% ', foreground='#000000', background='#81a1c1'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#B48EAD', background = "#81a1c1", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#B48EAD'),
                widget.DF(visible_on_warn=False, format='{uf}GB ', foreground='#000000', background='#B48EAD'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#EBcB8B',background = "#B48EAD", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=12, foreground='#000000', background='#EBcB8B'),
                widget.ThermalSensor(foreground='#000000',background='#EBcB8B', padding=9),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#A3BE8C',background = "#EBcB8B", padding=0, update_interval=60 ),
                widget.TextBox(text="", font="Font Awesome 5 Free", fontsize=11, foreground='#000000', background='#A3BE8C'),
                widget.Battery(format='{percent:2.0%} ', foreground='#000000', background='#A3BE8C'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#BF616a',background = "#A3BE8C", padding=0, update_interval=60 ),
                widget.Clock(format='   %B %d, %Y ', foreground='#000000', background='#BF616a'),
                widget.TextBox(text="\uE0B2", font="Font Awesome 5 Free", fontsize=28, foreground='#88c0d0', background = "#BF616a", padding=0, update_interval=60 ),
                widget.Clock(format='  %l:%M%p ', foreground='#000000', background='#88c0d0'),
                widget.CurrentLayoutIcon(custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")], scale=0.8, background='#81a1c1'),
                                          ],
            18,
            background='#00000000'
            ),
       )
 )

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus='#81a1c1',
    border_width=2,
    float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'), # gitk
    Match(wm_class='Nitrogen'),
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Tk'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

@hook.subscribe.startup_once
def start_once():
  home = os.path.expanduser('~')
  subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"
