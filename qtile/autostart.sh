#!/usr/bin/env bash

picom &

emacs --daemon &

nitrogen --restore &

lxpolkit &

eww daemon 

xbindkeys -f ~/.config/xbindkeys/config &
