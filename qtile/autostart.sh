#!/usr/bin/env bash

~/.screenlayout/resolution.sh

picom &

clipmenud

emacs --daemon

nitrogen --restore

mpd

#conky -c /home/legend/.config/conky/LegendLinux-nord.conkyrc

lxsession &

xbindkeys -f ~/.config/xbindkeys/config
