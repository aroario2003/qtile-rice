#!/bin/bash

killall -q polybar

while pgrep -x polybar >/dev/null; do
    sleep 1
done

polybar power-bar &
polybar workspaces &
polybar xwin-bar &
polybar clock-bar &
polybar date-bar &
polybar battery-bar &
polybar cputemp-bar &
polybar fs-bar &
polybar cpu-bar &
polybar vol-bar &
