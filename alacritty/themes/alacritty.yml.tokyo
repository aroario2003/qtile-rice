window:
  dimensions:
    columns: 100
    lines: 58

  position:
    x: 0
    y: 0

  padding:
    x: 7
    y: 10

  decorations: none

  startup_mode: Windowed

  title: Alacritty

  dynamic_title: true

  class:
    instance: Alacritty
    general: Alacritty

scrolling:
  history: 50000
  multiplier: 3

font:
  normal:
    family: Iosevka Nerd Font
    style: Regular
  bold:
    family: Iosevka Nerd Font
    style: Bold
  italic:
    family: Iosevka Nerd Font
    style: Italic
  bold_italic:
    family: Iosevka Nerd Font
    style: Bold Italic
  size: 8.0
  use_thin_strokes: false
draw_bold_text_with_bright_colors: false

colors:
  primary:
    background: '0x1a1b26'
    foreground: '0xa9b1d6'
    #dim_foreground: '#828482'
    #bright_foreground: '#eaeaea'
  cursor:
    text: CellBackground
    cursor: '#81a1c1'
  vi_mode_cursor:
    text: CellBackground
    cursor: '#81a1c1'
  search:
    matches:
      foreground: '#000000'
      background: '#ffffff'
    focused_match:
      foreground: CellBackground
      background: CellForeground
    bar:
      background: '#c5c8c6'
      foreground: '#1d1f21'
  line_indicator:
    foreground: None
    background: None
  selection:
    text: CellBackground
    background: CellForeground
  normal:
      black:   '0x32344a'
      red:     '0xf7768e'
      green:   '0x9ece6a'
      yellow:  '0xe0af68'
      blue:    '0x7aa2f7'
      magenta: '0xad8ee6'
      cyan:    '0x449dab'
      white:   '0x787c99'
  bright:
      black:   '0x444b6a'
      red:     '0xff7a93'
      green:   '0xb9f27c'
      yellow:  '0xff9e64'
      blue:    '0x7da6ff'
      magenta: '0xbb9af7'
      cyan:    '0x0db9d7'
      white:   '0xacb0d0'
    #dim:
  #  black:   '#131415'
  #  red:     '#864343'
  #  green:   '#777c44'
  #  yellow:  '#9e824c'
  #  blue:    '#556a7d'
  #  magenta: '#75617b'
  #  cyan:    '#5b7d78'
  #  white:   '#828482'
  #indexed_colors: []

window.opacity: 1.0

cursor:
  style:
    shape: Beam
    blinking: On
  vi_mode_style: None
  blink_interval: 750
  unfocused_hollow: true
  thickness: 0.2
live_config_reload: true
