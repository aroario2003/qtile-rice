window:
  dimensions:
    columns: 100
    lines: 58

  position:
    x: 0
    y: 0

  padding:
    x: 7
    y: 10

  decorations: none

  startup_mode: Windowed

  title: Alacritty

  dynamic_title: true

  class:
    instance: Alacritty
    general: Alacritty

scrolling:
  history: 50000
  multiplier: 3

font:
  normal:
    family: Iosevka Nerd Font
    style: Regular
  bold:
    family: Iosevka Nerd Font
    style: Bold
  italic:
    family: Iosevka Nerd Font
    style: Italic
  bold_italic:
    family: Iosevka Nerd Font
    style: Bold Italic
  size: 8.0
  use_thin_strokes: false
draw_bold_text_with_bright_colors: false

colors:
  primary:
   background: '#2E3440'
   foreground: '#D8DEF9'
    #dim_foreground: '#828482'
    #bright_foreground: '#eaeaea'
  cursor:
    text: CellBackground
    cursor: '#81a1c1'
  vi_mode_cursor:
    text: CellBackground
    cursor: '#81a1c1'
  search:
    matches:
      foreground: '#000000'
      background: '#ffffff'
    focused_match:
      foreground: CellBackground
      background: CellForeground
    bar:
      background: '#c5c8c6'
      foreground: '#1d1f21'
  line_indicator:
    foreground: None
    background: None
  selection:
    text: CellBackground
    background: CellForeground
  normal:
    black:   '#3B4252'
    red:     '#BF616a'
    green:   '#A3BE8C'
    yellow:  '#EBCB8B'
    blue:    '#81A1C1'
    magenta: '#B48EAD'
    cyan:    '#88C0D0'
    white:   '#E5E9F0'
  bright:
    black:   '#4C566A'
    red:     '#BF616A'
    green:   '#A3BE8C'
    yellow:  '#EBCB8B'
    blue:    '#81A1C1'
    magenta: '#B48EAD'
    cyan:    '#8FBCBB'
    white:   '#ECEFF4'
  #dim:
  #  black:   '#131415'
  #  red:     '#864343'
  #  green:   '#777c44'
  #  yellow:  '#9e824c'
  #  blue:    '#556a7d'
  #  magenta: '#75617b'
  #  cyan:    '#5b7d78'
  #  white:   '#828482'
  #indexed_colors: []

window.opacity: 1.0

cursor:
  style:
    shape: Beam
    blinking: On
  vi_mode_style: None
  blink_interval: 750
  unfocused_hollow: true
  thickness: 0.2
live_config_reload: true
