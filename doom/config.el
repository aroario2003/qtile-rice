(setq user-full-name "Alejandro Rosario"
      user-mail-address "borkbork1031@gmail.com")

(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 14 :weight 'medium))

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

;;(setq doom-theme 'everforest-hard-dark)

(use-package autothemer)

(load-theme 'doom-tokyo-night t)

(beacon-mode 1)

(setq display-line-numbers-type t)

;;get bigger markdown headers
(setq markdown-header-scaling t)

(setq visual-fill-column-width 110
      visual-fill-column-center-text t)

(setq org-directory "~/Documents/org/")

(setq frame-resize-pixelwise t)

;;ORG MODE
(require 'org-bullets)(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;;FORCE DASHBOARD
(setq org-hide-emphasis-markers t)

;;DIRED
(evil-define-key 'normal dired-mode-map
  (kbd "h" 'dired-up-directory)
  (kbd "j" 'dired-next-line)
  (kbd "k" 'dired-previous-line)
  (kbd "l" 'dired-open-file)
  (kbd "M" 'dired-do-chmod)
  (kbd "O" 'dired-do-chown))

;;KEYBINDINGS
(map! :leader
      :desc "file tree"
      "f t" #'neotree-toggle)
(map! :leader
      :desc "open magit"
      "g m" #'magit)
(map! :leader
      :desc "Activate peep-dired"
      "d p" #'peep-dired)
(map! :leader
      :desc "export org"
      "o o" #'org-babel-tangle)
(map! :leader
      :desc "start org-present"
      "o s" #'org-present)
(map! :leader
      :desc "stop org-present"
      "o S" #'org-present-quit)
(map! :leader
      :desc "toggle line numbers"
      "d l" #'display-line-numbers-mode)
(map! :leader
      :desc "write bookmarks"
      "b w" #'bookmark-write)

(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(custom-set-faces
'(hl-line ((t (:height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#7fbbb3" :height 4.0))))
 '(org-level-2 ((t (:foreground "#d699b6" :height 3.5))))
 '(org-level-3 ((t (:foreground "#83c092" :height 3.0))))
 '(org-level-4 ((t (:foreground "#a7c080" :height 2.5))))
 '(org-level-5 ((t (:foreground "#ddbc7f" :height 2.0))))
 '(org-level-6 ((t (:foreground "#e67e80" :height 1.5)))))

(use-package dashboard
  :init
  (setq dashboard-banner-logo-title "Emacs: The Best Elisp Interpreter")
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-init-info t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content nil)
  (setq dashboard-items '((recents . 5)
                          (agenda . 5)
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))
(setq doom-fallback-buffer-name "*dashboard*")

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

(defun my/org-present-start ()
    (doom-big-font-mode nil)
    (visual-fill-column-mode nil)
    (display-line-numbers-mode -1)
    (setq header-line-format " "))

(add-hook 'org-present-mode-hook 'my/org-present-start)

(defun my/org-present-end ()
    (doom-big-font-mode -1)
    (visual-fill-column-mode -1)
    (display-line-numbers-mode 1)
    (setq header-line-format nil))

(add-hook 'org-present-mode-quit-hook 'my/org-present-end)

;;Due to a bug where emacs has weird workspace behaviour
;;(after! persp-mode
;;  (setq persp-emacsclient-init-frame-behaviour-override "main"))
