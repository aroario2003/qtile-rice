;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Alejandro Rosario"
      user-mail-address "borkbork1031@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example::
;;
(setq doom-font (font-spec :family "Sauce Code Pro Nerd Font" :size 14 :weight 'medium))
;;      doom-variable-pitch-font (font-spec :family "Iosevka" :size 12))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))


;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

(beacon-mode 1)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;;get bigger markdown headers
(setq markdown-header-scaling t)

(setq visual-fill-column-width 110
      visual-fill-column-center-text t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/")

(setq frame-resize-pixelwise t)
;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;;ORG MODE
(require 'org-bullets)(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;;FORCE DASHBOARD
(setq org-hide-emphasis-markers t)

;;DIRED
(evil-define-key 'normal dired-mode-map
  (kbd "h" 'dired-up-directory)
  (kbd "j" 'dired-next-line)
  (kbd "k" 'dired-previous-line)
  (kbd "l" 'dired-open-file)
  (kbd "M" 'dired-do-chmod)
  (kbd "O" 'dired-do-chown))

;;KEYBINDINGS
(map! :leader
      :desc "file tree"
      "f t" #'neotree-toggle)
(map! :leader
      :desc "open magit"
      "g m" #'magit)
(map! :leader
      :desc "Activate peep-dired"
      "d p" #'peep-dired)
(map! :leader
      :desc "export org"
      "o o" #'org-babel-tangle)
(map! :leader
      :desc "start org-present"
      "o s" #'org-present)
(map! :leader
      :desc "stop org-present"
      "o S" #'org-present-quit)
(map! :leader
      :desc "toggle line numbers"
      "d l" #'display-line-numbers-mode)

(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(custom-set-faces
'(hl-line ((t (:height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#81a1c1" :height 4.0))))
 '(org-level-2 ((t (:foreground "#B48EAD" :height 3.5))))
 '(org-level-3 ((t (:foreground "#88c0d0" :height 3.0))))
 '(org-level-4 ((t (:foreground "#A3BE8C" :height 2.5))))
 '(org-level-5 ((t (:foreground "#EBCB8B" :height 2.0))))
 '(org-level-6 ((t (:foreground "#BF616a" :height 1.5)))))

(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content nil)
  (setq dashboard-items '((recents . 5)
                          (agenda . 5)
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))
(setq doom-fallback-buffer-name "*dashboard*")

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

(defun my/org-present-start ()
    (doom-big-font-mode nil)
    (visual-fill-column-mode nil)
    (display-line-numbers-mode -1)
    (setq header-line-format " "))

(add-hook 'org-present-mode-hook 'my/org-present-start)

(defun my/org-present-end ()
    (doom-big-font-mode -1)
    (visual-fill-column-mode -1)
    (display-line-numbers-mode 1)
    (setq header-line-format nil))

(add-hook 'org-present-mode-quit-hook 'my/org-present-end)

;;Due to a bug where emacs has weird workspace behaviour
(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))
