(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("2af6d337981b88f85980124e47e11cbff819200bba92648f59154a6ff35a7801" default))
 '(package-selected-packages '(autothemer exwm vimrc-mode selectric-mode minimap)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#7fbbb3" :height 4.0))))
 '(org-level-2 ((t (:foreground "#d699b6" :height 3.5))))
 '(org-level-3 ((t (:foreground "#83c092" :height 3.0))))
 '(org-level-4 ((t (:foreground "#a7c080" :height 2.5))))
 '(org-level-5 ((t (:foreground "#ddbc7f" :height 2.0))))
 '(org-level-6 ((t (:foreground "#e67e80" :height 1.5)))))
(put 'customize-face 'disabled nil)
