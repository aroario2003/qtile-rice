(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(vterm visual-fill-column org-present haskell-mode typescript-mode rainbow-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:extend t :background "#272C36" :height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#81a1c1" :height 4.0))))
 '(org-level-2 ((t (:foreground "#B48EAD" :height 3.5))))
 '(org-level-3 ((t (:foreground "#88c0d0" :height 3.0))))
 '(org-level-4 ((t (:foreground "#A3BE8C" :height 2.5))))
 '(org-level-5 ((t (:foreground "#EBCB8B" :height 2.0))))
 '(org-level-6 ((t (:foreground "#BF616a" :height 1.5)))))
(put 'customize-face 'disabled nil)
