//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
    {"  ", "cpu", 5, 0},

    {" ", "sensors | awk '/^temp1/ {print $2}' | sed -e 's/+//g'", 10, 0},

    {" ", "df -h | grep '/dev/sda2' | awk '{print $4}'", 60, 0 },

	{" ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	2,		0},

    {" ", "amixer sget Master | awk '{print $5}' | grep '%' | sed -e '1d' -e 's/\\]//g' -e 's/\\[//g'", 1, 0 },

    {" ", "acpi | awk '{print $4}' | sed 's/\\,//g'", 10, 0},

    {" ", "date '+%B %d, %Y'", 90, 0},

	{" ", "date '+%l:%M%p'",	 5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
