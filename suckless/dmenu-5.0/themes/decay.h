static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#101419" },
	[SchemeSel] = { "#000000", "#70a5eb" },
	[SchemeOut] = { "#000000", "#74bee9" },
};
