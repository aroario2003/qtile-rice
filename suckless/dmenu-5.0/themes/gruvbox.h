static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#282828" },
	[SchemeSel] = { "#000000", "#458588" },
	[SchemeOut] = { "#000000", "#689d6a" },
};

