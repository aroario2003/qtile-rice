static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#040c16" },
	[SchemeSel] = { "#000000", "#bdd6f4" },
	[SchemeOut] = { "#000000", "#b3ffff" },
};
