static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#2b3339" },
	[SchemeSel] = { "#000000", "#7fbbb3" },
	[SchemeOut] = { "#000000", "#00ffff" },
};

