static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#1a1b26" },
	[SchemeSel] = { "#000000", "#7aa2f7" },
	[SchemeOut] = { "#000000", "#449dab" },
};

