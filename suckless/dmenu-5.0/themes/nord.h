static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#2e3440" },
	[SchemeSel] = { "#000000", "#81a1c1" },
	[SchemeOut] = { "#000000", "#88c0d0" },
};

