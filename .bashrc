#
# ~/.bashrc
#

[[ $- != *i* ]] && return

[[ $- == *i* ]] && source ~/.local/share/blesh/ble.sh --noattach

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;33m\]\u\[\033[01;31m\]@\[\033[01;32m\h\[\033[01;36m\] \W\[\033[01;35m\] \$\[\033[00m\] '
	fi

	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

#set -o vi

unset use_color safe_term match_lhs sh

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less

xhost +local:root > /dev/null 2>&1

complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

shopt -s autocd

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend
#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

recordvidnoface() {
ffmpeg -f x11grab -thread_queue_size 64 -video_size 1920x1200 -framerate 30 -i :0.0 \
       -f pulse -i default \
       -vcodec libx264 -preset medium -qp 0 -pix_fmt yuv444p \
       $1
}

pyup() {
    pip3 install -U $(pip3 list --outdated | sed 1,2d | awk '{print $1}')
}

#aliases
alias ls="exa -a --group-directories-first --icons"
alias rf="ranger"
alias record="ffmpeg  -f x11grab -s 1920x1080 -i :0.0"
alias vim="nvim"
alias cd="z"
alias recordvid="ffmpeg -f x11grab -thread_queue_size 64 -video_size 1920x1200 -framerate 30 -i :0.0 \
       -f v4l2 -thread_queue_size 64 -video_size 320x180 -framerate 30 -i /dev/video0 \
       -f pulse -i default \
       -filter_complex 'overlay=main_w-overlay_w:main_h-overlay_h:format=yuv444' \
       -vcodec libx264 -preset ultrafast -qp 0 -pix_fmt yuv444p \
        video.mkv"
alias xbindkeys="xbindkeys -f "$XDG_CONFIG_HOME"/xbindkeys/config"
alias luamake="/home/legend/repos/lua-language-server/3rd/luamake/luamake"
alias lf="lfrun"

#export environment variables
export HIST_DEST='/home/hacker/history.txt'
export LC_ALL="en_US.UTF-8"
export PATH="/home/legend/go/bin/:/home/legend/.local/bin:/home/legend/.config/rofi/powermenu/:/home/legend/.cargo/bin:/home/legend/putils:$HOME/.emacs.d/bin:/home/legend/repos/lua-language-server/bin:/home/legend/.nimble/bin:/home/legend/.local/share/graalvm-ce-java19-22.3.1/bin:$PATH"
export EDITOR="nvim"
export KITTY_SHELL_INTEGRATION="no-cursor"
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc

#set vi mode for bash and start starship prompt
set -o vi
eval "$(starship init bash)"
eval "$(zoxide init bash)"

#ble.sh settings
ble-face -s auto_complete fg=grey
ble-face -s syntax_error fg=124
ble-bind -f 'C-u' 'insert-string "sudo pacman -Syu"'
[[ ${BLE_VERSION-} ]] && ble-attach

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

