#!/bin/bash

declare -a themes 
themes=("nord\n""gruvbox\n""everforest\n""tokyonight\n""articblush\n""dracula")
theme=$(echo -e ${themes[@]} | dmenu -n -p "Choose a theme:" -l 6)

if [ -z $theme ]; then 
    exit 1
fi

config="$HOME/.config/qtile/config.py"
current_scheme=$(grep "^colorscheme" $config)
sed -i "s/$current_scheme/colorscheme = $theme/g" "$config"
current_gtk2=$(grep "^gtk-theme-name" "$HOME/.gtkrc-2.0") 
current_gtk3=$(grep "^gtk-theme-name" "$HOME/.config/gtk-3.0/settings.ini")
sed -i "s/$current_gtk2/gtk-theme-name=\"$theme\"/g" "$HOME/.gtkrc-2.0"
sed -i "s/$current_gtk3/gtk-theme-name=$theme/g" "$HOME/.config/gtk-3.0/settings.ini"
sed -i "s/^colors: .*/colors: *$theme/g" "$HOME/.config/alacritty/alacritty.yml"

current_dmenu=$(grep "#include" "$HOME/suckless/dmenu-5.0/config.def.h" | awk -F '/' '{print $7}' | sed 's/"//g')
sed -i "s/$current_dmenu/$theme.h/g" "$HOME/suckless/dmenu-5.0/config.def.h"
cd $HOME/suckless/dmenu-5.0/
rm config.h
make
gksudo make install

current_vim=$(grep "colorscheme" "$HOME/.config/nvim/lua/setup/options.lua" | sed 1d | awk '{print $2}' | sed -e 's/]//g' -e 's/)//g')
sed -i "s/$current_vim/$theme/g" "$HOME/.config/nvim/lua/setup/options.lua"

current_lualine=$(grep "local colors" "$HOME/.config/nvim/lua/plugins/evil_lualine.lua")
sed -i "s/$current_lualine/local colors = $theme/g" "$HOME/.config/nvim/lua/plugins/evil_lualine.lua"

current_emacs=$(grep "load-theme '" "$HOME/.config/doom/config.el")
dunst_frame=$(grep "frame_color = *" "$HOME/.config/dunst/dunstrc" | sed 1q)
current_dunst=$(grep "background =" "$HOME/.config/dunst/dunstrc"| sed 1q)
if [ $theme == "nord" ]; then 
    nitrogen --set-scaled "$HOME/Pictures/nord/64a3x4pdubk51.png" --save 
    sed -i "s/$current_emacs/(load-theme 'doom-nord t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#81a1c1\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#2e3440\"/g" "$HOME/.config/dunst/dunstrc"
elif [ $theme == "gruvbox" ]; then 
    nitrogen --set-scaled "$HOME/Pictures/gruv/grub-coffee.png" --save 
    sed -i "s/$current_emacs/(load-theme 'doom-gruvbox t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#458588\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#282828\"/g" "$HOME/.config/dunst/dunstrc"
elif [ $theme == "everforest" ]; then 
    nitrogen --set-scaled "$HOME/Pictures/everforest/forest_stairs_everforest.jpg" --save
    sed -i "s/$current_emacs/(load-theme 'everforest-hard-dark t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#7fbbb3\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#2b3339\"/g" "$HOME/.config/dunst/dunstrc"
elif [ $theme == "tokyonight" ]; then 
    nitrogen --set-scaled "$HOME/Pictures/tokyo/squares.png" --save 
    sed -i "s/$current_emacs/(load-theme 'doom-tokyo-night t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#7aa2f7\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#1a1b26\"/g" "$HOME/.config/dunst/dunstrc"
elif [ $theme == "articblush" ]; then
    nitrogen --set-scaled "$HOME/Pictures/nord/16.png" --save 
    sed -i "s/$current_emacs/(load-theme 'articblush t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#bdd6f4\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#040c16\"/g" "$HOME/.config/dunst/dunstrc"
elif [ $theme == "dracula" ]; then 
    nitrogen --set-scaled "$HOME/Pictures/dracula/croppedarch.png" --save
    sed -i "s/$current_emacs/(load-theme 'doom-dracula t)/g" "$HOME/.config/doom/config.el"
    sed -i "s/$dunst_frame/    frame_color = \"#bd93f9\"/g" "$HOME/.config/dunst/dunstrc"
    sed -i "s/$current_dunst/    background = \"#282a36\"/g" "$HOME/.config/dunst/dunstrc"
fi

killall emacs 

if pgrep -x "dunst" > /dev/null 
then
    killall dunst
fi

emacs --daemon

eww --restart daemon

pkill -SIGUSR1 python

