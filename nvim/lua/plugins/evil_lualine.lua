-- Eviline config for lualine
-- Author: shadmansaleh
-- Credit: glepnir
local lualine = require('lualine')

-- Color table for highlights
-- stylua: ignore
local everforest = {
  bg       = '#2b3339',
  fg       = '#d3c6aa',
  yellow   = '#ddbc7f',
  cyan     = '#83c092',
  darkblue = '#3a94c5',
  green    = '#a7c080',
  orange   = '#e69875',
  violet   = '#df69ba',
  magenta  = '#d699b6',
  blue     = '#7fbbb3',
  red      = '#e67e80',
}

local tokyonight = {
    bg       = '#1a1b26',
    fg       = '#a9b1d6',
    yellow   = '#e0af68',
    cyan     = '#449dab',
    darkblue = '#7aa2f7',
    green    = '#9ece6a',
    orange   = '#e0af68',
    violet   = '#ad8ee6',
    magenta  = '#ad8ee6',
    blue     = '#7aa2f7',
    red      = '#f7768e',
}

local gruvbox = {
    bg       = '#282828',
    fg       = '#ebdbb2',
    red      = '#cc241d',
    green    = '#98971a',
    yellow   = '#d79921',
    blue     = '#458588',
    magenta  = '#b16286',
    cyan     = '#689d6a',
    violet   = '#b16286',
    orange   = '#d79921',
    darkblue = '#458588',
}

local nord = {
    bg       = '#2e3440',
    fg       = '#d8dee9',
    red      = '#bf616a',
    green    = '#a3be8c',
    yellow   = '#ebcb8b',
    blue     = '#81a1c1',
    magenta  = '#b48ead',
    cyan     = '#88c0d0',
    violet   = '#b48ead',
    orange   = '#ebcb8b',
    darkblue = '#81a1c1',
}

local articblush = {
    bg       = '#040c16',
    fg       = '#cce9ea',
    red      = '#ff7377',
    green    = '#aaf0c1',
    yellow   = '#eadd94',
    blue     = '#bdd6f4',
    magenta  = '#f9ecf7',
    cyan     = '#b3ffff',
    orange   = '#eadd94',
    violet   = '#f9ecf7',
    darkblue = '#bdd6f4'
}

local decay = {
    bg       = '#101419',
    fg       = '#b6beca',
    red      = '#e05f65',
    blue     = '#70a5eb',
    yellow   = '#f1cf8a',
    magenta  = '#c68aee',
    cyan     = '#74bee9',
    orange   = '#f1cf8a',
    violet   = '#c68aee',
    darkblue = '#70a5eb',
}

local dracula = {
    bg       = '#282a36',
    fg       = '#f8f8f2',
    red      = '#ff5555',
    blue     = '#bd93f9',
    yellow   = '#f1fa8c',
    magenta  = '#ff79c6',
    cyan     = '#8be9fd',
    orange   = '#f1fa8c',
    violet   = '#ff79c6',
    darkblue = '#bd93f9',
}

local colors = tokyonight

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand('%:t')) ~= 1
  end,
  hide_in_width = function()
    return vim.fn.winwidth(0) > 80
  end,
  check_git_workspace = function()
    local filepath = vim.fn.expand('%:p:h')
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,
}

-- Config
local config = {
  options = {
    -- Disable sections and component separators
    component_separators = '',
    section_separators = '',
    theme = {
      normal = { c = { fg = colors.fg, bg = colors.bg } },
      inactive = { c = { fg = colors.fg, bg = colors.bg } },
    },
  },
  sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
  inactive_sections = {
    -- these are to remove the defaults
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
}

-- Inserts a component in lualine_c at left section
local function ins_left(component)
  table.insert(config.sections.lualine_c, component)
end

-- Inserts a component in lualine_x ot right section
local function ins_right(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
  function()
    return '▊'
  end,
  color = { fg = colors.blue }, -- Sets highlighting of component
  padding = { left = 0, right = 1 }, -- We don't need space before this
}

ins_left {
  -- mode component
  function()
    return ''
  end,
  color = function()
    -- auto change color according to neovims mode
    local mode_color = {
      n = colors.green,
      i = colors.red,
      v = colors.blue,
      [''] = colors.blue,
      V = colors.blue,
      c = colors.magenta,
      no = colors.red,
      s = colors.orange,
      S = colors.orange,
      [''] = colors.orange,
      ic = colors.yellow,
      R = colors.violet,
      Rv = colors.violet,
      cv = colors.red,
      ce = colors.red,
      r = colors.cyan,
      rm = colors.cyan,
      ['r?'] = colors.cyan,
      ['!'] = colors.red,
      t = colors.red,
    }
    return { fg = mode_color[vim.fn.mode()] }
  end,
  padding = { right = 1 },
}

ins_left {
  -- filesize component
  'filesize',
  cond = conditions.buffer_not_empty,
}

ins_left {
  'filename',
  cond = conditions.buffer_not_empty,
  color = { fg = colors.magenta, gui = 'bold' },
}

ins_left { 'location' }

ins_left { 'progress', color = { fg = colors.fg, gui = 'bold' } }

ins_left {
  'diagnostics',
  sources = { 'nvim_diagnostic' },
  symbols = { error = ' ', warn = ' ', info = ' ' },
  diagnostics_color = {
    color_error = { fg = colors.red },
    color_warn = { fg = colors.yellow },
    color_info = { fg = colors.cyan },
  },
}

ins_left {
  function()
    return '%='
  end,
}

ins_left {
  function()
    local msg = 'No Active Lsp'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients()
    if next(clients) == nil then
      return msg
    end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = '  LSP:',
  color = { fg = '#e69875', gui = 'bold' },
}

-- Add components to right sections
ins_right {
  'o:encoding',
  fmt = string.upper,
  cond = conditions.hide_in_width,
  color = { fg = colors.green, gui = 'bold' },
}

ins_right {
  'fileformat',
  fmt = string.upper,
  icons_enabled = true,
  color = { fg = colors.yellow, gui = 'bold' },
}

ins_right {
  'branch',
  icon = '',
  color = { fg = colors.violet, gui = 'bold' },
}

ins_right {
  'diff',
  -- Is it me or the symbol for modified us really weird
  symbols = { added = ' ', modified = '柳 ', removed = ' ' },
  diff_color = {
    added = { fg = colors.green },
    modified = { fg = colors.orange },
    removed = { fg = colors.red },
  },
  cond = conditions.hide_in_width,
}

ins_right {
  function()
    return '▊'
  end,
  color = { fg = colors.blue },
  padding = { left = 1 },
}

lualine.setup(config)
