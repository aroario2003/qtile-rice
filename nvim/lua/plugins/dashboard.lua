local h = vim.highlight

--configure dashboard--
local db = require('dashboard')
db.custom_header = {
            '',
            '███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
            '████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║',
            '██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║',
            '██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
            '██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
            '╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
            '                                               ',
            '',
            }
db.custom_center = {
  { 
     icon = '  ', 
     desc = 'last session                         ', 
     shortcut = 'SPC s l', 
     action = ':SessionLoad' 
  },
  { 
      icon = '  ', 
      desc = 'find recent files                    ', 
      shortcut = 'SPC f r', 
      action = ':History' 
  },
  { 
      icon = '  ', 
      desc = 'find file                            ', 
      shortcut = 'SPC f f', 
      action = ':Telescope find_files' 
  },
  { 
      icon = 'ﱐ  ', 
      desc = 'new file                             ', 
      shortcut = 'SPC c n', 
      action = ':DashboardNewFile' 
  },
  { 
      icon = '  ', 
      desc = 'change colorscheme                   ', 
      shortcut = 'SPC t c', 
      action = ':Colors' 
  },
  { 
      icon = '  ', 
      desc = 'find word                            ', 
      shortcut = 'SPC f a', 
      action = ':Rg' 
  },
  { 
      icon = '  ', 
      desc = 'bookmarks                            ', 
      shortcut = 'SPC f b', 
      action = ':Marks' 
  }
}

--get number of plugins loaded--
local plugins_count = vim.fn.len(
    vim.fn.globpath('~/.local/share/nvim/site/pack/packer/start', '*', 0, 1)
)

db.custom_footer = {
  'Neovim loaded '.. plugins_count ..' plugins'
}

--highlight config--
--[[ h.create('dashboardHeader', {ctermfg=6, guifg='#7fbbb3'})
h.create('dashboardCenter', {ctermfg=4, guifg='#d699b6'})
h.create('dashboardFooter', {ctermfg=6, guifg='#83c092'}) ]]
