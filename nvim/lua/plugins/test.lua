local pop_buf = vim.api.nvim_create_buf(false, true)
local win_id = vim.api.nvim_open_win(pop_buf, true, {
    relative='editor',
    width=30,
    height=30,
    row=10,
    col=10,
    style='minimal',
})

vim.api.nvim_buf_set_lines(pop_buf, 0, -1, false, {"hello world!"})

vim.api.nvim_win_set_option(win_id, 'winblend', 0)

vim.api.nvim_buf_set_option(pop_buf, 'modifiable', false)
