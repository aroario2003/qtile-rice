local lsp = require('lspconfig')

require("mason").setup()
require("mason-lspconfig").setup()



--treesitter config--
require'nvim-treesitter.configs'.setup {
  ensure_installed = { "lua", "java", "python", "rust" },
  sync_install = false,
  auto_install = true,
  ignore_install = { "javascript" },
  highlight = {
    enable = true,
    disable = { "" },
    additional_vim_regex_highlighting = false,
  },
  rainbow = {
    enable = true,
    extended_mode = true,
    max_file_lines = nil,
  },
  autopairs = {
      enable = true,
  }
 }

--lspconfig setup--
lsp.pyright.setup{}
lsp.html.setup{}
lsp.eslint.setup{}
lsp.tsserver.setup{}
lsp.zls.setup{}
lsp.ols.setup{}
lsp.jdtls.setup{
  cmd = { 'jdtls' },
   root_dir = function(fname)
      return require'lspconfig'.util.root_pattern('pom.xml', 'gradle.build', '.git')(fname) or vim.fn.getcwd()
   end
}
lsp.rust_analyzer.setup{}

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', 'ca', vim.lsp.buf.code_action, opts)
  end,
})
