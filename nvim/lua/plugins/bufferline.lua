--set up bufferline--
require('bufferline').setup {
    options = {
        icon_seperator_active = '▎',
        icon_seperator_inactive = '▎',
        buffer_close_icon = '',
        modified_icon = '●',
        close_icon = '',
        left_trunc_marker = '',
        right_trunc_marker = '',
        offsets = { 
            { 
            filetype = "NvimTree", 
            text = "", 
            highlight = "Directory", 
            text_align = "center", 
            padding = 1
            }
        },
    }
}
