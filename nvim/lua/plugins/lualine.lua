local everforest = require'lualine.themes.everforest'

--custom lualine colors--
everforest.normal.a.bg = '#2b3339'
everforest.normal.a.fg = '#7fbbb3'
everforest.insert.a.bg = '#2b3339'
everforest.insert.a.fg = '#d699b6'
everforest.normal.c.bg = '#2b3339'
everforest.insert.c.bg = '#2b3339'
everforest.normal.c.fg = '#a7c080'
everforest.insert.c.fg = '#a7c080'
everforest.normal.b.bg = '#2b3339'
everforest.insert.b.bg = '#2b3339'

--custom lualine components--
local function vline()
  return '▊'
end

local function custom_mode()
  return ''
end

--lualine setup--
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'tokyonight',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {"NvimTree"},
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {vline, custom_mode},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location', 'fileformat', vline}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}
