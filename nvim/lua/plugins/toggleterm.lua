require('toggleterm').setup()

local Terminal = require('toggleterm.terminal').Terminal
local ranger = Terminal:new({ cmd = "ranger", hidden = true })

function ranger_toggle() 
    ranger:toggle()
end

