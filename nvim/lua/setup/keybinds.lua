local A = vim.api
local g = vim.g

--map the leader key--
g.mapleader = ' '
g.maplocalleader = ' '

--dashboard center keybindings--
A.nvim_set_keymap('n', '<leader>ff', ':Telescope find_files<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fh', ':History<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>tc', ':Colors<CR>', {noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fa', ':Rg<CR>', {noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fb', ':Marks<CR>', {noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fr', ':History<CR>', { noremap = true, silent = true })

--move lines in visual mode--
A.nvim_set_keymap('v', 'J', ':move +1<CR>gv-gv', { noremap = true, silent = true })
A.nvim_set_keymap('v', 'K', ':move -2<CR>gv-gv', { noremap = true, silent = true })

--buffer controls--
A.nvim_set_keymap('n', '<leader>bh', ':BufferLineCyclePrev<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>bl', ':BufferLineCycleNext<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>bc', ':BufferClose<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>tn', ':tabnew<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>bk', ':BufferLinePickClose<CR>', { noremap = true, silent = true })

--disable arrow keys because theu suck--
A.nvim_set_keymap('n', '<up>', '<nop>', { noremap = true })
A.nvim_set_keymap('n', '<down>', '<nop>', { noremap = true })
A.nvim_set_keymap('n', '<left>', '<nop>', { noremap = true })
A.nvim_set_keymap('n', '<right>', '<nop>', { noremap = true })

--window movement--
A.nvim_set_keymap('n', '<leader>wh', ':wincmd h<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>wj', ':wincmd j<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>wk', ':wincmd k<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>wl', ':wincmd l<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>wv', ':vs<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>ws', ':sp<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>wc', ':q<CR>', { noremap = true, silent = true })

--NvimTree keybindings--
A.nvim_set_keymap('n', '<leader>tt', ':NvimTreeToggle<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>tf', ':NvimTreeFocus<CR>', { noremap = true, silent = true})

-- toggleterm keybindings--
A.nvim_set_keymap('n', '<leader>oV', ':ToggleTerm direction="vertical" size=100<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>oH', ':ToggleTerm direction="horizontal" size=20<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fm', ':lua ranger_toggle()<CR>', { noremap = true, silent = true })

--Undotree keybindings--
A.nvim_set_keymap('n', '<leader>ut', ":UndotreeToggle<CR>", { noremap = true, silent = true })

--miscellanious bindings--
A.nvim_set_keymap('n', '<leader>sp', ':set spell!<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>fs', ':w<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>ft', ':FloatermToggle<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>sf', ':so %<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>gk', ':Telescope help_tags<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<C-d>', '<C-d>zz', {noremap=true, silent=true})
A.nvim_set_keymap('n', '<C-u>', '<C-u>zz', {noremap=true, silent=true})

--lsp bindings--
A.nvim_set_keymap('n', 'K', ':lua vim.lsp.buf.hover()<CR>', { noremap = true, silent = true })
A.nvim_set_keymap('n', '<leader>e', ':lua vim.diagnostic.open_float()<CR>', { noremap = true, silent = true })
