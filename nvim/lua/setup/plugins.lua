--setup plugins--
return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use 'glepnir/dashboard-nvim'
    use "rebelot/kanagawa.nvim"   
    use 'lewis6991/impatient.nvim'
    use 'mbbill/undotree'
    use 'mfussenegger/nvim-jdtls'
    use 'mfussenegger/nvim-dap'
     use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }
    use { 
        "catppuccin/nvim",
        as = "catppuccin",
        config = function()
            require("catppuccin").setup {
                flavour = "mocha"
            }
        end
    }
    use { "ellisonleao/gruvbox.nvim" }
    use 'shaunsingh/nord.nvim'
    use {'articblush/articblush.nvim', as = 'articblush'}
    use {'decaycs/decay.nvim', as = 'decay'}
    use 'Mofiqul/dracula.nvim'
    use 'folke/tokyonight.nvim'
    use 'L3MON4D3/LuaSnip'
    use 'voldikss/vim-floaterm'
    use 'RRethy/vim-illuminate'
    use 'windwp/nvim-autopairs'
    use 'neovim/nvim-lspconfig'
    use 'akinsho/toggleterm.nvim'
    use 'kyazdani42/nvim-tree.lua'
    use 'numToStr/Comment.nvim'
    use {
        'akinsho/bufferline.nvim',
        tag = "v2.*",
        requires = 'kyazdani42/nvim-web-devicons'
    }
    use 'jiangmiao/auto-pairs'
    use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-vsnip'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/vim-vsnip'
    use 'junegunn/fzf.vim'
    use 'junegunn/vim-easy-align'
    use {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",

    }
    use 'unblevable/quick-scope'
    use 'norcalli/nvim-colorizer.lua'
    use {
      'nvim-telescope/telescope.nvim',
      requires = { 'nvim-lua/plenary.nvim', opt = true }
    }
    use 'sainnhe/everforest'
    use 'folke/which-key.nvim'
    use {
      'nvim-lualine/lualine.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }
    use 'lervag/vimtex'
end)

    
