local map = vim.api.nvim_set_keymap
local view = require "nvim-tree.view"

function toggle_tree()
  if view.win_open() then
    require "bufferline.state".set_offset(0)
    require "nvim-tree".close()
  else
    require "bufferline.state".set_offset(31, "File Explorer")
    require "nvim-tree".find_file(true)
  end
end
