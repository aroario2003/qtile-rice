call plug#begin('~/.vim/plugged')
let g:vimtex_view_method = 'zathura'
let g:airline_right_sep = ''
let g:airline_left_sep = ''
let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
      \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" },
      \ 'enable': {
      \    'tabline': 0,
      \ },
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly' ] ],
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ]]
      \ },
      \ 'tabline': {
      \   'right': [ ['close'], ['filetype'] ]
      \ }
      \ }

set termguicolors
set number
set relativenumber
syntax on
filetype plugin on
set nocompatible
let g:tablineclosebutton=0
let g:dashboard_default_executive ='telescope'
let g:lightline.colorscheme = 'nord'
let bufferline = get(g:, 'bufferline', {})
let bufferline.icon_separator_active = '▎'
let bufferline.icon_separator_inactive = '▎'
let bufferline.icon_close_tab = ''
let bufferline.icon_close_tab_modified = '●'
let bufferline.no_name_title = v:null
let g:dashboard_custom_header = [
            \'',
            \'███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
            \'████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║',
            \'██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║',
            \'██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
            \'██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
            \'╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
            \'                                               ',
            \'',
            \]

" Make sure you use single quotes
 Plug 'junegunn/seoul256.vim'
 Plug 'junegunn/vim-easy-align'
 Plug 'elixir-editors/vim-elixir'

" Group dependencies, vim-snippets depends on ultisnips
 Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
 Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
 Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'lervag/vimtex'
Plug 'ghifarit53/tokyonight-vim'
" Using git URL
Plug 'iamcco/markdown-preview.nvim'
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
 Plug 'https://github.com/junegunn/vim-github-dashboard.git'
 Plug 'https://github.com/github/copilot.vim.git'
 Plug 'https://github.com/ap/vim-css-color.git'
 Plug 'https://github.com/vim-syntastic/syntastic.git'
 "  Plug 'https://github.com/vim-airline/vim-airline.git'
 "Plug 'https://github.com/vim-airline/vim-airline-themes.git'
 "Plug 'https://github.com/mhinz/vim-startify.git'
 Plug 'https://github.com/liuchengxu/vim-which-key.git'
 Plug 'https://github.com/francoiscabrol/ranger.vim.git'
 Plug 'https://github.com/junegunn/fzf.vim.git'
 Plug 'https://github.com/voldikss/vim-floaterm.git'
Plug 'https://github.com/morhetz/gruvbox.git' 
 Plug 'https://github.com/vimwiki/vimwiki.git'
 Plug 'https://github.com/unblevable/quick-scope.git'
 Plug 'https://github.com/jiangmiao/auto-pairs.git'
 Plug 'neoclide/coc.nvim', {'branch': 'release'}
 "Plug 'https://github.com/itchyny/lightline.vim.git'
 Plug 'https://github.com/kyazdani42/nvim-web-devicons.git'
 Plug 'https://github.com/airblade/vim-gitgutter.git'
 Plug 'nvim-lualine/lualine.nvim'
 "Plug 'https://github.com/kdheepak/tabline.nvim.git'
 Plug 'nvim-lua/plenary.nvim'
 Plug 'nvim-telescope/telescope.nvim'
 Plug 'glepnir/dashboard-nvim'
 Plug 'danilamihailov/beacon.nvim'
 Plug 'https://github.com/arcticicestudio/nord-vim.git'
 Plug 'romgrk/barbar.nvim'
 Plug 'zah/nim.vim'
 Plug 'akinsho/toggleterm.nvim'
 Plug 'kyazdani42/nvim-tree.lua'
" Plugin option
 Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
" Plugin outside ~/.vim/plugged with post-update hook
 Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
" Unmanaged plugin (manually installed and updated)
 Plug '~/my-prototype-plugin'

 let g:dashboard_custom_shortcut={
\ 'last_session'       : 'SPC s l',
\ 'find_history'       : 'SPC f h',
\ 'find_file'          : 'SPC f f',
\ 'new_file'           : 'SPC c n',
\ 'change_colorscheme' : 'SPC t c',
\ 'find_word'          : 'SPC f a',
\ 'book_marks'         : 'SPC f b',
\ }

call plug#end() 
let g:NERDTreeDirArrowExpandable = ''
let g:beacon_enable = 1
let g:beacon_minimal_jump = 0
let g:NERDTreeDirArrowCollapsible = ''
set t_Co=256
set expandtab
set clipboard=unnamedplus
set shiftwidth=4 
set tabstop=4
set hidden
set mouse=a
set ignorecase
set ttyfast 
set timeout timeoutlen=1000 ttimeoutlen=50
set incsearch
set scrolloff=8
set laststatus=2
set background=dark
set showtabline=2
colorscheme nord
 
set colorcolumn=150
"highlight dashboardHeader ctermfg=6 guifg=#458588
"highlight dashboardShortCut ctermfg=4 guifg=#689d6a
"highlight dashboardCenter ctermfg=6 guifg=#689d6a
"highlight dashboardFooter ctermfg=4 guifg=#458588
"highlight WhichKeySeperator guifg=#458588

let vim_markdown_preview_browser='Brave Browser'
let g:nvim_tree_git_hl = 1 
let g:nvim_tree_highlight_opened_files = 1 
let g:nvim_tree_root_folder_modifier = ':~' 
let g:nvim_tree_add_trailing = 1 
let g:nvim_tree_group_empty = 1 
let g:nvim_tree_icon_padding = ' ' 
let g:nvim_tree_symlink_arrow = ' >> ' 
let g:nvim_tree_respect_buf_cwd = 1 
let g:nvim_tree_create_in_closed_folder = 0 
let g:nvim_tree_refresh_wait = 500 

let g:nvim_tree_special_files = { 'README.md': 1, 'Makefile': 1, 'MAKEFILE': 1 } " List of filenames that gets highlighted with NvimTreeSpecialFile
let g:nvim_tree_show_icons = {
    \ 'git': 0,
    \ 'folders': 1,
    \ 'files': 1,
    \ 'folder_arrows': 0,
    \ }

let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged': "✗",
    \   'staged': "✓",
    \   'unmerged': "",
    \   'renamed': "➜",
    \   'untracked': "★",
    \   'deleted': "",
    \   'ignored': "◌"
    \   },
    \ 'folder': {
    \   'arrow_open': "",
    \   'arrow_closed': "",
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   }
    \ }

lua require('nvim-tree').setup()
source ~/.config/nvim/user/lualine.lua
source ~/.config/nvim/user/bufferline.lua
let mapleader = "\<Space>"
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap <silent> <leader> :WhichKey ' '<CR>
nnoremap <leader>fr :Ranger<CR>
nnoremap <leader>tt :ToggleTerm<CR>
nnoremap <leader>sp :set spell!<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <silent> <leader>t :lua toggle_tree()<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>nf :NvimTreeFindFile<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>'
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
nnoremap <leader>x :silent !chmod +x %<CR>
nnoremap <silent> tn :tabnew<CR>
nnoremap <silent> bc :BufferClose<CR>
nnoremap <leader>o :Files<CR>
nnoremap bh :BufferPrevious<CR>
nnoremap bl :BufferNext<CR>
nnoremap <silent> <leader>cn :DashboardNewFile<CR>
nmap <leader>sl :<C-u>SessionLoad<CR>
nmap <leader>ss :<C-u>SessionSave<CR>
nnoremap <silent> <Leader>fh :History<CR>
nnoremap <silent> <Leader>ff :Telescope find_files<CR>
nnoremap <silent> <Leader>tc :Colors<CR>
nnoremap <silent> <Leader>fa :Rg<CR>
nnoremap <silent> <Leader>fb :Marks<CR>
