--files to config neovim--
require('setup.keybinds')
require('setup.options')
require('setup.plugins')

--files to config plugins for neovim--
require('plugins.autopairs')
require('plugins.treesitter')
require('plugins.bufferline')
require('plugins.colorizer')
require('plugins.cmp')
require('plugins.comment')
require('plugins.dashboard')
require('plugins.dap')
require('plugins.evil_lualine')
require('plugins.impatient')
require('plugins.nvimtree')
require('plugins.toggleterm')
require('plugins.whichkey')
