monitor=,1920x1200@60,0x0,1

exec-once = hyprpaper 
exec-once = dunst 
exec-once = lxpolkit 
exec-once = emacs --daemon 
exec-once = eww daemon 
exec-once = waybar
exec-once = /home/legend/.config/hypr/scripts/xdg-portal-hyprland
exec-once = xprop -root -f _XWAYLAND_GLOBAL_OUTPUT_SCALE 32c -set _XWAYLAND_GLOBAL_OUTPUT_SCALE 2
exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = foot --server

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true
    enable_swallow = true
    swallow_regex = ^(foot)$
}


input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 
}

general {

    gaps_in = 10
    gaps_out = 15
    border_size = 2
    col.active_border = rgba(7aa2f7aa) rgba(ad8ee6aa) rgba(449dabaa) 45deg
    col.inactive_border = rgba(00000000)

    layout = master
}

decoration {

    rounding = 10
    blur = yes
    blur_xray = true
    blur_size = 3
    blur_passes = 3
    blur_new_optimizations = on

    active_opacity = 0.9
    inactive_opacity = 0.9

    drop_shadow = yes
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    bezier=overshot,0.13,0.99,0.29,1.1   
    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, overshot, slide
    animation = windowsOut, 1, 5, default, popin 80%
    animation = border, 1, 10, default
    animation = fade, 1, 5, default
    animation = workspaces, 1, 5, default, slidevert
}

dwindle {
    pseudotile = yes 
    preserve_split = yes 
}

master {
    new_is_master = true
}

gestures {
    workspace_swipe = off
}

device:epic mouse V1 {
    sensitivity = -0.5
}

windowrulev2 = workspace 2, class:^(brave-browser)$
windowrulev2 = workspace 4, class:^(Emacs)$
windowrulev2 = workspace 5, class:^(virt-manager)$
windowrulev2 = opaque, class:^(brave-browser)$
windowrulev2 = opaque, class:^(Emacs)$
windowrulev2 = opaque, class:^(thunar)$
windowrulev2 = opaque, class:^(virt-manager)$
windowrulev2 = opaque, class:^(discord)$

$mainMod = SUPER

bind = $mainMod, Return, exec, foot
bind = $mainMod SHIFT, Return, exec, /home/legend/.config/hypr/scripts/bemenu 
bind = $mainMod, Escape, exec, eww open-many powermenu messagewin
bind = $mainMod, Q, killactive, 
bind = $mainMod SHIFT, Q, exit, 
bind = $mainMod, F, exec, thunar
bind = $mainMod, Space, togglefloating, 
bind = $mainMod, B, exec, brave
bind = $mainMod SHIFT, N, exec, footclient -e newsboat
bind = $mainMod, V, exec, virt-manager
bind = $mainMod, E, exec, emacs

bind = $mainMod SHIFT, F, fullscreen, 0

bind = , xf86audioraisevolume, exec, /home/legend/.local/bin/volume.sh up
bind = , xf86audiolowervolume, exec, /home/legend/.local/bin/volume.sh down
bind = , xf86audiomute, exec, /home/legend/.local/bin/volume.sh mute

bind = $mainMod, h, movefocus, l
bind = $mainMod, l, movefocus, r
bind = $mainMod, k, movefocus, u
bind = $mainMod, j, movefocus, d

bind = $mainMod SHIFT, h, movewindow, l 
bind = $mainMod SHIFT, l, movewindow, r 
bind = $mainMod SHIFT, j, movewindow, d 
bind = $mainMod SHIFT, k, movewindow, u 

bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

bind = $mainMod, R, submap, resize
submap = resize
binde = , l, resizeactive, 10 0
binde = , h, resizeactive, -10 0
binde = , k, resizeactive, 0 -10
binde = , j, resizeactive, 0 10
bind = , escape, submap, reset
submap = reset

bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
